import _ from 'lodash';
import Storage from './storage';

const API_VERSION = '0.0.12';
const KEY_API_VERSION = 'topo-api-version';
const KEY_TOPOLOGY_LIST = 'topo-list';
const KEY_TOPOLOGY_NEXT_ID = 'topo-next-id';

export default class TopologyApi {
  constructor(keyId = 'id') {
    if (Storage.get(KEY_API_VERSION) !== API_VERSION) {
      Storage.set(KEY_API_VERSION, API_VERSION);
      Storage.remove(KEY_TOPOLOGY_LIST);
      Storage.remove(KEY_TOPOLOGY_NEXT_ID);
    }
    this._keyId = keyId;
    this._nextId = Storage.get(KEY_TOPOLOGY_NEXT_ID) || 1;
  }

  getTopologyList() {
    return Storage.get(KEY_TOPOLOGY_LIST) || [];
  }

  getTopology(id) {
    const list = this.getTopologyList();
    let where = {};
    where[this._keyId] = id;
    return _.find(list, where);
  }

  storeTopology(data) {
    data[this._keyId] = this._nextId;
    Storage.set(KEY_TOPOLOGY_NEXT_ID, this._nextId += 1);

    let list = this.getTopologyList();
    list.push(data);
    Storage.set(KEY_TOPOLOGY_LIST, list);
    return data;
  }

  updateTopology(id, newData) {
    let list = this.getTopologyList();
    let where = {};
    where[this._keyId] = id;
    let data = _.find(list, where);
    if (!data) {
      throw new Error(`topology not found: ${id}`);
    }
    _.assign(data, newData);
    Storage.set(KEY_TOPOLOGY_LIST, list);
    return data;
  }

  removeTopology(id) {
    let list = this.getTopologyList();
    let where = {};
    where[this._keyId] = id;
    let data = _.remove(list, where).pop();
    Storage.set(KEY_TOPOLOGY_LIST, list);
    return data;
  }

  clearAllTopology() {
    Storage.remove(KEY_TOPOLOGY_LIST);
  }
}
