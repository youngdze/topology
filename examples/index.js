import _ from 'lodash';
import AppsTopology from 'apps-topology';
import './style.scss';
import angular from 'angular';
import 'angular-ui-bootstrap';
import 'angular-aside';
import $ from 'jquery';
import topoData from './faked';

angular.module('topo', [
  'ui.bootstrap',
  'ngAside',
])

.constant('NODETYPES', [
  {
    name: 'TCP',
    value: 'Sphere'
  }, {
    name: 'HTTP',
    value: 'Sphere'
  }, {
    name: 'RCP',
    value: 'Cube'
  }, {
    name: 'RD',
    value: 'Cube'
  }, {
    name: 'NoSql',
    value: 'Cylinder'
  }, {
    name: 'Cache',
    value: 'Cylinder'
  }, {
    name: 'Files',
    value: 'Cylinder'
  }
])

.service('TopoUtil', [
  function() {
    return {
      assemUserData(topoData, fixed) {
        if (_.isUndefined(fixed)) fixed = true;

        let nodes = _.map(topoData.nodes, function(node) {
          return _.assignIn({}, {
            name: node.name,
            capacity: node.capacity,
          }, node.style);
        });

        let links = _.map(topoData.links, 'style');
        let areas = _.map(topoData.areas, 'style');
        let notes = _.map(topoData.notes, 'style');

        return {
          appId: 1,
          topoId: topoData.id,
          fixed, nodes, links, areas, notes
        };
      }
    };
  }
])

.controller('TopoCtrl', ['$scope', '$uibModal', '$aside', '$location', '$timeout', 'TopoUtil', 'NODETYPES',
  ($scope, $uibModal, $aside, $location, $timeout, TopoUtil, NODETYPES) => {
    const pageStatus = 'edit';

    // init
    let topo;

    $scope.themeLight = true;
    $scope.creating = false;
    $scope.pageStatus = pageStatus;
    $scope.loading = pageStatus === 'create';
    $scope.topo = {
      id: (pageStatus === 'edit') ? topoData.id : '',
      name: (pageStatus === 'edit') ? topoData.name : '',
      description: (pageStatus === 'edit') ? topoData.name : ''
    };
    $scope.validateTopo = {
      name: true,
      description: true
    };
    $scope.appendingType = null;

    $scope.clearInputStatus = function(type) {
      $scope.validateTopo[type] = true;
    };

    // topo editor methods
    $scope.nodeTypes = NODETYPES;
    $scope.asideOpened = false;
    $scope.asideData = {};
    // 暂存 note
    $scope.currentNote = '';

    $scope.canUndo = false;
    $scope.canRedo = false;

    $scope.zoomVal = 1;
    $scope.zoomTopo = function(type) {
      let topoEditor = $('#topo-canvas svg');
      if (type === 'in') {
        $scope.zoomVal = $scope.zoomVal * 1.2;
      } else if (type === 'out') {
        $scope.zoomVal = $scope.zoomVal * 0.8;
      }
      topoEditor.css('transform', 'scale(' + $scope.zoomVal + ')');
    };

    $scope.closeAside = function() {
      $scope.asideOpened = false;
    };

    let appsTopologyData = {};
    if (pageStatus === 'create') {
      appsTopologyData = TopoUtil.assemBaseData(topoData, false);
    } else {
      appsTopologyData = TopoUtil.assemUserData(topoData);
    }

    let canvasWidth = $('#topo-canvas').width(),
        canvasHeight = window.innerHeight,
        topoSize = {width: canvasWidth, height: canvasHeight};

    let keyId = 'appId';
    let appsTopologyOptions = {
      appendTo: '#topo-canvas',
      keyId,
      nodeFields: ['name'],
      size: [canvasWidth, canvasHeight],
      linkDistance: 100,
      baseHref: $location.url(),
      hoverEnabled: false,
      autoScaleToFitCanvas: true
    };
    topo = new AppsTopology(appsTopologyData, appsTopologyOptions);
    $(window).on('resize', function(ev) {
      let canvasWidth = $('#topo-canvas').width(),
          canvasHeight = window.innerHeight;
      topo.setSize([canvasWidth, canvasHeight]);
      topoSize = {width: canvasWidth, height: canvasHeight};
    });

    let setScope = function(key, val) {
      if (_.eq($scope[key], val)) return;

      let phase = $scope.$$phase;
      if (phase !== '$apply' && phase !== '$digest') {
        $scope.$apply(function() {
          $scope[key] = val;
        });
      }
      $scope[key] = val;
    };

    $scope.zoomRate = 1;
    $scope.zoomTopo = function(type) {
      let zoomRateRange = topo.getScaleRange();

      if (type === 'in') {
        $scope.zoomRate = Math.floor($scope.zoomRate / 5 * 10) / 10 * 5 + 5 / 10;
      } else if (type === 'out') {
        $scope.zoomRate = Math.ceil($scope.zoomRate / 5 * 10) / 10 * 5 - 5 / 10;
      }

      if ($scope.zoomRate < zoomRateRange[0]) $scope.zoomRate = zoomRateRange[0];
      if ($scope.zoomRate > zoomRateRange[1]) $scope.zoomRate = zoomRateRange[1];

      topo.setCanvasScale($scope.zoomRate);
    };

    $scope.topoToggleFullscreen = function() {
      let isFullscreen = !$scope.isFullscreen,
          $topoWrapper = document.querySelector('.topos-wrapper'),
          windowWidth = window.outerWidth,
          windowHeight = window.outerHeight;
      if (isFullscreen) {
        $topoWrapper.webkitRequestFullscreen && $topoWrapper.webkitRequestFullscreen();
        $topoWrapper.mozRequestFullScreen && $topoWrapper.mozRequestFullScreen();
        $topoWrapper.msRequestFullscreen && $topoWrapper.msRequestFullscreen();
        $topoWrapper.requestFullscreen && $topoWrapper.requestFullscreen();

        topo.setSize([windowWidth, windowHeight]);
        $scope.isFullscreen = isFullscreen;
      } else {
        document.webkitExitFullscreen && document.webkitExitFullscreen();
        document.mozCancelFullScreen && document.mozCancelFullScreen();
        document.msExitFullscreen && document.msExitFullscreen();
        document.exitFullscreen && document.exitFullscreen();

        $timeout(function() {
          topo.setSize([topoSize.width, topoSize.height]);
          $scope.isFullscreen = isFullscreen;
        }, 500);
      }
    };

    topo.on('forceEnd', function() {
      setScope('loading', false);
    });

    topo.on('activeChange', function(type, node) {
      if (node) {
        setScope('asideData', angular.extend({}, {type, node}));

        if (type === 'link') setScope('asideOpened', true);
        if (type === 'note') setScope('currentNote', node.content);
      } else {
        setScope('asideOpened', false);
        setScope('asideData', {});
      }
    });

    topo.on('stateChange', function(state) {
      switch (state) {
        case AppsTopology.STATE_DRAGGING_NODE:
        case AppsTopology.STATE_DRAGGING_NOTE:
        case AppsTopology.STATE_DRAGGING_AREA:
          setScope('asideOpened', false);
          break;
        case AppsTopology.STATE_DRAG_END:
          setScope('asideOpened', true);
          break;
        default:
          break;
      }

      switch (state) {
        case AppsTopology.STATE_LINKING:
          setScope('appendingType', 'link');
          break;
        case AppsTopology.STATE_DRAWING_AREA:
          setScope('appendingType', 'area');
          break;
        case AppsTopology.STATE_DRAWING_NOTE:
          setScope('appendingType', 'note');
          break;
        case AppsTopology.STATE_DRAWING_NODE:
          setScope('appendingType', 'node');
          break;
        case AppsTopology.STATE_ZOOMING_CANVAS:
          setScope('zoomRate', topo.getScaleLevel());
          break;
        default:
          setScope('appendingType', null);
          break;
      }
    });

    topo.on('history', function() {
      setScope('canUndo', topo.history.canUndo());
      setScope('canRedo', topo.history.canRedo());
    });

    $scope.undo = function() {
      topo.undo();
    };
    $scope.redo = function() {
      topo.redo();
    };

    $scope.append = function(type) {
      switch (type) {
        case 'node':
          let node = {
            appId: Math.random().toString(16).substr(2),
            name: `应用${Math.random().toString(16).substr(2, 4)}`,
            shape: 'Cube',
            x: topo.options.size[0] / 2,
            y: topo.options.size[1] / 2
          };
          topo.appendNode(node);
          topo.setActive('node', node);

          $scope.asideData = angular.extend({}, {type, node});
          $scope.asideOpened = true;
          break;
        case 'link':
          topo.toLink();
          break;
        case 'area':
          topo.toAppendArea();
          break;
        case 'note':
          topo.toAppendNote();
          break;
        default:
          break;
      }
    };

    $scope.changeNodeType = function(type, label) {
      if ($scope.asideData.node && type) {
        topo.setNodeShape(undefined, type, label);
      }
    };

    $scope.changeNote = function(note) {
      topo.setNoteContent(undefined, note);
    };

    $scope.remove = function() {
      let type = $scope.asideData.type;
      switch (type) {
        case 'node':
          topo.removeNode();
          break;
        case 'link':
          topo.unlink();
          break;
        case 'area':
          topo.removeArea();
          break;
        case 'note':
          topo.removeNote();
          break;
        default:
          break;
      }
      $scope.asideOpened = false;
    };

    $scope.switchTheme = function(theme) {
      $scope.themeLight = theme === 'light';
    };
  }
])

.filter('percentage', [
  '$filter',
  function ($filter) {
    return (input, decimals) => {
      return $filter('number')(input * 100, decimals) + '%';
    };
  }
])

;
