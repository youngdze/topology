const _storage = window.localStorage;

export default class Storage {
  static get(key) {
    let list = _storage.getItem(key) || null;
    if (list) {
      list = JSON.parse(list);
    }
    return list;
  }

  static set(key, value) {
    _storage.setItem(key, JSON.stringify(value));
    return this;
  }

  static remove(key) {
    _storage.removeItem(key);
    return this;
  }

  static clear() {
    _storage.clear();
    return this;
  }
}
