export default {
  "description": "Topology",
  "areas": [],
  "name": "Topology",
  "links": [{
    "source": "5763d0f017909",
    "style": {
      "source": "5763d0f017909",
      "target": "56e7c02de54d7"
    },
    "target": "56e7c02de54d7"
  }, {
    "source": "5763d0f017909",
    "style": {
      "source": "5763d0f017909",
      "target": "570c78fb93f4a"
    },
    "target": "570c78fb93f4a"
  }, {
    "source": "5763d0f017909",
    "style": {
      "source": "5763d0f017909",
      "target": "5732f736337cf"
    },
    "target": "5732f736337cf"
  }, {
    "source": "5763d0f017909",
    "style": {
      "source": "5763d0f017909",
      "target": "56ea615d44b87"
    },
    "target": "56ea615d44b87"
  }, {
    "source": "5763d0f017909",
    "style": {
      "source": "5763d0f017909",
      "target": "5721b0251492a"
    },
    "target": "5721b0251492a"
  }, {
    "source": "5763d0f017909",
    "style": {
      "source": "5763d0f017909",
      "target": "579f047da722b"
    },
    "target": "579f047da722b"
  }, {
    "source": "5763d0f017909",
    "style": {
      "source": "5763d0f017909",
      "target": "575cd3477f53e"
    },
    "target": "575cd3477f53e"
  }, {
    "source": "5763d0f017909",
    "style": {
      "source": "5763d0f017909",
      "target": "56dff11497f66"
    },
    "target": "56dff11497f66"
  }, {
    "source": "5763d0f017909",
    "style": {
      "source": "5763d0f017909",
      "target": "56fe2267cd27c"
    },
    "target": "56fe2267cd27c"
  }, {
    "source": "5763d0f017909",
    "style": {
      "source": "5763d0f017909",
      "target": "587712d1c19ff"
    },
    "target": "587712d1c19ff"
  }, {
    "source": "5732f736337cf",
    "style": {
      "source": "5732f736337cf",
      "target": "5763d0f017909"
    },
    "target": "5763d0f017909"
  }, {
    "source": "5732f736337cf",
    "style": {
      "source": "5732f736337cf",
      "target": "575cd3477f53e"
    },
    "target": "575cd3477f53e"
  }, {
    "source": "5732f736337cf",
    "style": {
      "source": "5732f736337cf",
      "target": "56dff11497f66"
    },
    "target": "56dff11497f66"
  }, {
    "source": "5732f736337cf",
    "style": {
      "source": "5732f736337cf",
      "target": "579f047da722b"
    },
    "target": "579f047da722b"
  }, {
    "source": "5732f736337cf",
    "style": {
      "source": "5732f736337cf",
      "target": "5721b0251492a"
    },
    "target": "5721b0251492a"
  }, {
    "source": "5732f736337cf",
    "style": {
      "source": "5732f736337cf",
      "target": "56ea615d44b87"
    },
    "target": "56ea615d44b87"
  }, {
    "source": "5732f736337cf",
    "style": {
      "source": "5732f736337cf",
      "target": "570a16f18bb4b"
    },
    "target": "570a16f18bb4b"
  }, {
    "source": "5732f736337cf",
    "style": {
      "source": "5732f736337cf",
      "target": "570dc9e5e769e"
    },
    "target": "570dc9e5e769e"
  }, {
    "source": "5732f736337cf",
    "style": {
      "source": "5732f736337cf",
      "target": "56e7c02de54d7"
    },
    "target": "56e7c02de54d7"
  }, {
    "source": "5732f736337cf",
    "style": {
      "source": "5732f736337cf",
      "target": "570c78fb93f4a"
    },
    "target": "570c78fb93f4a"
  }, {
    "source": "5732f736337cf",
    "style": {
      "source": "5732f736337cf",
      "target": "587712d1c19ff"
    },
    "target": "587712d1c19ff"
  }, {
    "source": "570a16f18bb4b",
    "style": {
      "source": "570a16f18bb4b",
      "target": "5763d0f017909"
    },
    "target": "5763d0f017909"
  }, {
    "source": "570a16f18bb4b",
    "style": {
      "source": "570a16f18bb4b",
      "target": "575cd3477f53e"
    },
    "target": "575cd3477f53e"
  }, {
    "source": "570a16f18bb4b",
    "style": {
      "source": "570a16f18bb4b",
      "target": "56dff11497f66"
    },
    "target": "56dff11497f66"
  }, {
    "source": "570a16f18bb4b",
    "style": {
      "source": "570a16f18bb4b",
      "target": "579f047da722b"
    },
    "target": "579f047da722b"
  }, {
    "source": "575cd3477f53e",
    "style": {
      "source": "575cd3477f53e",
      "target": "5721b0251492a"
    },
    "target": "5721b0251492a"
  }, {
    "source": "56dff11497f66",
    "style": {
      "source": "56dff11497f66",
      "target": "579f047da722b"
    },
    "target": "579f047da722b"
  }, {
    "source": "56dff11497f66",
    "style": {
      "source": "56dff11497f66",
      "target": "56fe2267cd27c"
    },
    "target": "56fe2267cd27c"
  }, {
    "source": "56dff11497f66",
    "style": {
      "source": "56dff11497f66",
      "target": "587712d1c19ff"
    },
    "target": "587712d1c19ff"
  }, {
    "source": "5721b0251492a",
    "style": {
      "source": "5721b0251492a",
      "target": "56fe2267cd27c"
    },
    "target": "56fe2267cd27c"
  }, {
    "source": "5721b0251492a",
    "style": {
      "source": "5721b0251492a",
      "target": "56ea615d44b87"
    },
    "target": "56ea615d44b87"
  }, {
    "source": "5721b0251492a",
    "style": {
      "source": "5721b0251492a",
      "target": "587712d1c19ff"
    },
    "target": "587712d1c19ff"
  }, {
    "source": "56e7c02de54d7",
    "style": {
      "source": "56e7c02de54d7",
      "target": "570a16f18bb4b"
    },
    "target": "570a16f18bb4b"
  }, {
    "source": "56e7c02de54d7",
    "style": {
      "source": "56e7c02de54d7",
      "target": "570dc9e5e769e"
    },
    "target": "570dc9e5e769e"
  }, {
    "source": "56e7c02de54d7",
    "style": {
      "source": "56e7c02de54d7",
      "target": "570c78fb93f4a"
    },
    "target": "570c78fb93f4a"
  }, {
    "source": "56e7c02de54d7",
    "style": {
      "source": "56e7c02de54d7",
      "target": "5732f736337cf"
    },
    "target": "5732f736337cf"
  }, {
    "source": "56e7c02de54d7",
    "style": {
      "source": "56e7c02de54d7",
      "target": "587712d1c19ff"
    },
    "target": "587712d1c19ff"
  }, {
    "source": "570c78fb93f4a",
    "style": {
      "source": "570c78fb93f4a",
      "target": "5763d0f017909"
    },
    "target": "5763d0f017909"
  }, {
    "source": "570c78fb93f4a",
    "style": {
      "source": "570c78fb93f4a",
      "target": "575cd3477f53e"
    },
    "target": "575cd3477f53e"
  }, {
    "source": "570c78fb93f4a",
    "style": {
      "source": "570c78fb93f4a",
      "target": "56dff11497f66"
    },
    "target": "56dff11497f66"
  }, {
    "source": "570c78fb93f4a",
    "style": {
      "source": "570c78fb93f4a",
      "target": "579f047da722b"
    },
    "target": "579f047da722b"
  }, {
    "source": "570c78fb93f4a",
    "style": {
      "source": "570c78fb93f4a",
      "target": "5721b0251492a"
    },
    "target": "5721b0251492a"
  }, {
    "source": "570c78fb93f4a",
    "style": {
      "source": "570c78fb93f4a",
      "target": "56ea615d44b87"
    },
    "target": "56ea615d44b87"
  }, {
    "source": "570c78fb93f4a",
    "style": {
      "source": "570c78fb93f4a",
      "target": "570a16f18bb4b"
    },
    "target": "570a16f18bb4b"
  }, {
    "source": "570c78fb93f4a",
    "style": {
      "source": "570c78fb93f4a",
      "target": "570dc9e5e769e"
    },
    "target": "570dc9e5e769e"
  }, {
    "source": "56ea615d44b87",
    "style": {
      "source": "56ea615d44b87",
      "target": "56e7c02de54d7"
    },
    "target": "56e7c02de54d7"
  }, {
    "source": "56ea615d44b87",
    "style": {
      "source": "56ea615d44b87",
      "target": "570c78fb93f4a"
    },
    "target": "570c78fb93f4a"
  }, {
    "source": "56ea615d44b87",
    "style": {
      "source": "56ea615d44b87",
      "target": "5732f736337cf"
    },
    "target": "5732f736337cf"
  }, {
    "source": "56ea615d44b87",
    "style": {
      "source": "56ea615d44b87",
      "target": "5763d0f017909"
    },
    "target": "5763d0f017909"
  }, {
    "source": "56ea615d44b87",
    "style": {
      "source": "56ea615d44b87",
      "target": "575cd3477f53e"
    },
    "target": "575cd3477f53e"
  }, {
    "source": "56ea615d44b87",
    "style": {
      "source": "56ea615d44b87",
      "target": "56dff11497f66"
    },
    "target": "56dff11497f66"
  }, {
    "source": "56ea615d44b87",
    "style": {
      "source": "56ea615d44b87",
      "target": "579f047da722b"
    },
    "target": "579f047da722b"
  }, {
    "source": "56ea615d44b87",
    "style": {
      "source": "56ea615d44b87",
      "target": "5721b0251492a"
    },
    "target": "5721b0251492a"
  }, {
    "source": "56ea615d44b87",
    "style": {
      "source": "56ea615d44b87",
      "target": "56fe2267cd27c"
    },
    "target": "56fe2267cd27c"
  }, {
    "source": "56ea615d44b87",
    "style": {
      "source": "56ea615d44b87",
      "target": "587712d1c19ff"
    },
    "target": "587712d1c19ff"
  }],
  "creator": "youngdze",
  "default": false,
  "notes": [],
  "diff": {
    "remove_edges": [],
    "add_nodes": [{
      "id": "5884584aec0d3"
    }, {
      "id": "588177cde7dba"
    }, {
      "id": "5883050a35b57"
    }],
    "add_edges": [{
      "source": "5763d0f017909",
      "target": "570a16f18bb4b"
    }, {
      "source": "5763d0f017909",
      "target": "570dc9e5e769e"
    }, {
      "source": "5763d0f017909",
      "target": "5883050a35b57"
    }, {
      "source": "5763d0f017909",
      "target": "5884584aec0d3"
    }, {
      "source": "588177cde7dba",
      "target": "5763d0f017909"
    }, {
      "source": "588177cde7dba",
      "target": "5732f736337cf"
    }, {
      "source": "588177cde7dba",
      "target": "575cd3477f53e"
    }, {
      "source": "588177cde7dba",
      "target": "56dff11497f66"
    }, {
      "source": "588177cde7dba",
      "target": "579f047da722b"
    }, {
      "source": "588177cde7dba",
      "target": "587712d1c19ff"
    }, {
      "source": "588177cde7dba",
      "target": "5721b0251492a"
    }, {
      "source": "588177cde7dba",
      "target": "56e7c02de54d7"
    }, {
      "source": "588177cde7dba",
      "target": "570c78fb93f4a"
    }, {
      "source": "588177cde7dba",
      "target": "56ea615d44b87"
    }, {
      "source": "570a16f18bb4b",
      "target": "5883050a35b57"
    }, {
      "source": "570a16f18bb4b",
      "target": "5884584aec0d3"
    }, {
      "source": "570dc9e5e769e",
      "target": "5883050a35b57"
    }, {
      "source": "570dc9e5e769e",
      "target": "5884584aec0d3"
    }, {
      "source": "575cd3477f53e",
      "target": "570a16f18bb4b"
    }, {
      "source": "575cd3477f53e",
      "target": "570dc9e5e769e"
    }, {
      "source": "575cd3477f53e",
      "target": "5883050a35b57"
    }, {
      "source": "575cd3477f53e",
      "target": "5884584aec0d3"
    }, {
      "source": "56dff11497f66",
      "target": "570a16f18bb4b"
    }, {
      "source": "56dff11497f66",
      "target": "570dc9e5e769e"
    }, {
      "source": "56dff11497f66",
      "target": "5883050a35b57"
    }, {
      "source": "56dff11497f66",
      "target": "5884584aec0d3"
    }, {
      "source": "579f047da722b",
      "target": "570a16f18bb4b"
    }, {
      "source": "579f047da722b",
      "target": "570dc9e5e769e"
    }, {
      "source": "579f047da722b",
      "target": "5883050a35b57"
    }, {
      "source": "579f047da722b",
      "target": "5884584aec0d3"
    }, {
      "source": "587712d1c19ff",
      "target": "5732f736337cf"
    }, {
      "source": "587712d1c19ff",
      "target": "56e7c02de54d7"
    }, {
      "source": "587712d1c19ff",
      "target": "570c78fb93f4a"
    }, {
      "source": "587712d1c19ff",
      "target": "5883050a35b57"
    }, {
      "source": "587712d1c19ff",
      "target": "5884584aec0d3"
    }, {
      "source": "5721b0251492a",
      "target": "5883050a35b57"
    }, {
      "source": "5721b0251492a",
      "target": "5884584aec0d3"
    }, {
      "source": "5732f736337cf",
      "target": "5883050a35b57"
    }, {
      "source": "5732f736337cf",
      "target": "5884584aec0d3"
    }, {
      "source": "56e7c02de54d7",
      "target": "5883050a35b57"
    }, {
      "source": "56e7c02de54d7",
      "target": "5884584aec0d3"
    }, {
      "source": "570c78fb93f4a",
      "target": "5732f736337cf"
    }, {
      "source": "570c78fb93f4a",
      "target": "587712d1c19ff"
    }, {
      "source": "570c78fb93f4a",
      "target": "56e7c02de54d7"
    }, {
      "source": "570c78fb93f4a",
      "target": "5883050a35b57"
    }, {
      "source": "570c78fb93f4a",
      "target": "5884584aec0d3"
    }, {
      "source": "56ea615d44b87",
      "target": "570a16f18bb4b"
    }, {
      "source": "56ea615d44b87",
      "target": "570dc9e5e769e"
    }, {
      "source": "56ea615d44b87",
      "target": "5883050a35b57"
    }, {
      "source": "56ea615d44b87",
      "target": "5884584aec0d3"
    }, {
      "source": "5883050a35b57",
      "target": "5763d0f017909"
    }, {
      "source": "5883050a35b57",
      "target": "5732f736337cf"
    }, {
      "source": "5883050a35b57",
      "target": "570a16f18bb4b"
    }, {
      "source": "5883050a35b57",
      "target": "570dc9e5e769e"
    }, {
      "source": "5883050a35b57",
      "target": "575cd3477f53e"
    }, {
      "source": "5883050a35b57",
      "target": "56dff11497f66"
    }, {
      "source": "5883050a35b57",
      "target": "579f047da722b"
    }, {
      "source": "5883050a35b57",
      "target": "587712d1c19ff"
    }, {
      "source": "5883050a35b57",
      "target": "5721b0251492a"
    }, {
      "source": "5883050a35b57",
      "target": "56e7c02de54d7"
    }, {
      "source": "5883050a35b57",
      "target": "570c78fb93f4a"
    }, {
      "source": "5883050a35b57",
      "target": "56ea615d44b87"
    }, {
      "source": "5883050a35b57",
      "target": "56fe2267cd27c"
    }, {
      "source": "5884584aec0d3",
      "target": "5763d0f017909"
    }, {
      "source": "5884584aec0d3",
      "target": "5732f736337cf"
    }, {
      "source": "5884584aec0d3",
      "target": "570a16f18bb4b"
    }, {
      "source": "5884584aec0d3",
      "target": "570dc9e5e769e"
    }, {
      "source": "5884584aec0d3",
      "target": "575cd3477f53e"
    }, {
      "source": "5884584aec0d3",
      "target": "56dff11497f66"
    }, {
      "source": "5884584aec0d3",
      "target": "579f047da722b"
    }, {
      "source": "5884584aec0d3",
      "target": "587712d1c19ff"
    }, {
      "source": "5884584aec0d3",
      "target": "56fe2267cd27c"
    }, {
      "source": "5884584aec0d3",
      "target": "5721b0251492a"
    }, {
      "source": "5884584aec0d3",
      "target": "56e7c02de54d7"
    }, {
      "source": "5884584aec0d3",
      "target": "570c78fb93f4a"
    }, {
      "source": "5884584aec0d3",
      "target": "56ea615d44b87"
    }],
    "remove_nodes": []
  },
  "nodes": [{
    "style": {
      "y": 365.48780682571,
      "x": 657.67169642658,
      "shape": "Cube",
      "transform": {
        "skew": 0,
        "translate": [0, 0],
        "rotate": 0,
        "scale": [1, 1]
      },
      "appId": "5763d0f017909"
    },
    "id": "5763d0f017909",
    "name": "应用1"
  }, {
    "style": {
      "y": 409.39469961477,
      "x": 800.21545191169,
      "shape": "Cube",
      "transform": {
        "skew": 0,
        "translate": [0, 0],
        "rotate": 0,
        "scale": [1, 1]
      },
      "appId": "56e7c02de54d7"
    },
    "id": "56e7c02de54d7",
    "name": "应用2"
  }, {
    "style": {
      "y": 299.28186147904,
      "x": 934.93050783415,
      "shape": "Cube",
      "transform": {
        "skew": 0,
        "translate": [0, 0],
        "rotate": 0,
        "scale": [1, 1]
      },
      "appId": "570c78fb93f4a"
    },
    "id": "570c78fb93f4a",
    "name": "应用3"
  }, {
    "style": {
      "y": 633.65583373089,
      "x": 807.51598872161,
      "shape": "Cube",
      "transform": {
        "skew": 0,
        "translate": [0, 0],
        "rotate": 0,
        "scale": [1, 1]
      },
      "appId": "5732f736337cf"
    },
    "id": "5732f736337cf",
    "name": "应用4"
  }, {
    "style": {
      "y": 539.94866885026,
      "x": 764.87460108657,
      "shape": "Cube",
      "transform": {
        "skew": 0,
        "translate": [0, 0],
        "rotate": 0,
        "scale": [1, 1]
      },
      "appId": "56ea615d44b87"
    },
    "id": "56ea615d44b87",
    "name": "应用4"
  }, {
    "style": {
      "y": 472.45386368324,
      "x": 936.91258320306,
      "shape": "Cube",
      "transform": {
        "skew": 0,
        "translate": [0, 0],
        "rotate": 0,
        "scale": [1, 1]
      },
      "appId": "5721b0251492a"
    },
    "id": "5721b0251492a",
    "name": "应用5"
  }, {
    "style": {
      "y": 118.67689543412,
      "x": 855.68700628037,
      "shape": "Cube",
      "transform": {
        "skew": 0,
        "translate": [0, 0],
        "rotate": 0,
        "scale": [1, 1]
      },
      "appId": "579f047da722b"
    },
    "id": "579f047da722b",
    "name": "应用6"
  }, {
    "style": {
      "y": 667.7585281649,
      "x": 673.17519915949,
      "shape": "Cube",
      "transform": {
        "skew": 0,
        "translate": [0, 0],
        "rotate": 0,
        "scale": [1, 1]
      },
      "appId": "575cd3477f53e"
    },
    "id": "575cd3477f53e",
    "name": "应用7"
  }, {
    "style": {
      "y": 311.28186147904,
      "x": 496.77366748368,
      "shape": "Cube",
      "transform": {
        "skew": 0,
        "translate": [0, 0],
        "rotate": 0,
        "scale": [1, 1]
      },
      "appId": "56dff11497f66"
    },
    "id": "56dff11497f66",
    "name": "应用8"
  }, {
    "style": {
      "y": 232.2414718351,
      "x": 690.83572069311,
      "shape": "Cube",
      "transform": {
        "skew": 0,
        "translate": [0, 0],
        "rotate": 0,
        "scale": [1, 1]
      },
      "appId": "56fe2267cd27c"
    },
    "id": "56fe2267cd27c",
    "name": "应用9"
  }, {
    "style": {
      "y": 474.35354168953,
      "x": 690.83572069311,
      "shape": "Cube",
      "transform": {
        "skew": 0,
        "translate": [0, 0],
        "rotate": 0,
        "scale": [1, 1]
      },
      "appId": "587712d1c19ff"
    },
    "id": "587712d1c19ff",
    "name": "应用10"
  }, {
    "style": {
      "y": 527.92834478368,
      "x": 531.08741679694,
      "shape": "Cube",
      "transform": {
        "skew": 0,
        "translate": [0, 0],
        "rotate": 0,
        "scale": [1, 1]
      },
      "appId": "570a16f18bb4b"
    },
    "id": "570a16f18bb4b",
    "name": "应用11"
  }, {
    "style": {
      "y": 625.80480184173,
      "x": 934.93050783415,
      "shape": "Cube",
      "transform": {
        "skew": 0,
        "translate": [0, 0],
        "rotate": 0,
        "scale": [1, 1]
      },
      "appId": "570dc9e5e769e"
    },
    "id": "570dc9e5e769e",
    "name": "应用11"
  }],
  "type": "app",
  "id": "5881bb660a69392fb0c9fa37",
  "ctime": 1484897126
};
