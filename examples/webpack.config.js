// eslint-disable-next-line
'use strict';

let webpack = require('webpack');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let isProduction = (process.env.NODE_ENV === 'production');

let plugins = [
  /*new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor', // Move dependencies to our main file
    filename: 'vendor.js',
    // children: true, // Look for common dependencies in all children,
    minChunks: Infinity, // How many times a dependency must come up before being extracted
  }),*/
  new ExtractTextPlugin('style.css')
];
if (isProduction) {
  plugins.push(new webpack.optimize.UglifyJsPlugin({
    compress: {
      // drop_console: true,
      warnings: false
    }
  }));
}

module.exports = {
  entry: {
    // vendor: ['d3', 'lodash'],
    examples: [`${__dirname}/index.js`]
  },
  output: {
    path: `${__dirname}`,
    filename: 'index-build.js',
    publicPath: 'examples/'
  },
  externals: {
    d3: 'd3',
    lodash: '_',
    'apps-topology': 'AppsTopology'
  },
  devtool: 'source-map',
  plugins,
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        include: [
          `${__dirname}`
        ]
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader'),
        // loader: 'style!css!sass',
        include: `${__dirname}`,
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: [
            'file?name=[hash].[ext]',
            'image-webpack'
        ]
      }
    ]
  },
};
