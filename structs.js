let topologyData = {
  nodes: [
    {
      id,   // primary key
      name, // display as text
      otherAttributes,
      ...
    },
    ...
  ],
  links: [
    {
      source, // node id
      target  // node id
    },
    ...
  ]
};

let viewData = {
  nodes: [
    {
      id,
      name,
      otherAttributes,

      // view attributes
      shape,
      x,
      y,
      textPosition,
      otherViewAttributes,

      ...
    },
    ...
  ],
  links: [
    {
      source, // node id
      target  // node id
    },
    ...
  ],
  areas: [
    {
      x,
      y,
      width,
      height,
      ...
    },
    ...
  ],
  notes: [
    {
      content,
      x,
      y,
      arrowDirection,
      ...
    }
  ]
};
