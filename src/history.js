/**
 * Create a manageable history of states (or actions).
 *
 * @author Wang Shenwei
 */
export default class History {

  /**
   * @constructor
   * @param {Object} options `options.maxStates` set the maximum states to record.
   */
  constructor({maxStates = 100} = {}) {
    this._history = [];
    this._cursor = -1;
    this._maxStates = maxStates;
  }

  /**
   * Get history length.
   *
   * @getter
   * @returns {number}
   */
  get length() {
    return this._history.length;
  }

  /**
   * Get current state.
   *
   * @returns {*}
   */
  current() {
    return this._history[this._cursor] || null;
  }

  /**
   * Push a new state.
   *
   * @param {*} state
   * @returns {History} Self
   */
  push(state) {
    this._history.splice(this._cursor += 1);
    while (this._history.length && this._history.length >= this._maxRecord) {
      this._cursor -= 1;
      this._history.shift();
    }
    this._history.push(state);
    return this;
  }

  /**
   * Replace by a new state.
   *
   * @param {*} state
   * @returns {History} Self
   */
  replace(state) {
    this._history.splice(this._cursor, 1, state);
    return this;
  }

  /**
   * Go back and set cursor to the previous state.
   *
   * @returns {*} Returns the previous state, or null if not found.
   */
  back() {
    if (!this.canBack()) {
      return null;
    }
    return this._history[this._cursor -= 1] || null;
  }

  /**
   * Go forward and set cursor to the next state.
   *
   * @returns {*} Returns the next state, or null if not found.
   */
  forward() {
    if (!this.canForward()) {
      return null;
    }
    return this._history[this._cursor += 1] || null;
  }

  /**
   * Go forward and set cursor to the state with the specified offset.
   *
   * @param {number} offset
   * @returns {*} Returns the specified state, or null if not found.
   */
  go(offset = 0) {
    let newCursor = this._cursor + offset;
    if (newCursor < -1 || newCursor > this._history.length - 1) {
      return null;
    }
    return this._history[this._cursor = newCursor] || null;
  }

  /**
   * Detect if there were a previous state.
   *
   * @returns {boolean}
   */
  canBack() {
    return this._cursor >= 0;
  }

  /**
   * Detect if there were a next state.
   *
   * @returns {boolean}
   */
  canForward() {
    return this._cursor < this._history.length - 1;
  }

  /**
   * Get the current state (as the previous action) (to undo),
   * and set cursor to the previous state.
   *
   * @returns {*} Returns the previous action, or null if not found.
   */
  undo() {
    let state = this.current();
    this.back();
    return state;
  }

  /**
   * Get the next state (as the next action) (to redo),
   * and set cursor to the next state.
   *
   * @returns {*} Returns the next action, or null if not found.
   */
  redo() {
    return this.forward();
  }

  /**
   * Detect if there were a previous action to undo.
   *
   * @returns {boolean}
   */
  canUndo() {
    return !!this.current();
  }

  /**
   * Detect if there were a next action to redo.
   *
   * @returns {boolean}
   */
  canRedo() {
    return this.canForward();
  }
}
