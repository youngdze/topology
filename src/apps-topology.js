import EventEmitter from 'events';
import _ from 'lodash';
import * as d3 from 'd3';
import History from './history';
import * as nodeShapes from './node-shape';
import NoteShape from './note-shape';
import Mousetrap from '../externals/mousetrap';
import './styles/style.scss';

const DEFAULT_OPTIONS = {
  keyId: 'id',
  nodeFields: [],
  linkFields: [],
  linkStartOffset: 5.5,
  linkEndOffset: 7,
  linkMirrorGap: 12,
  linkTextOffset: 12,
  clearOnePxLink: true,
  guideThreshold: 10,
  initialized: false,
  historyMaxStates: Infinity,
  resizeHandleSize: 10,
  resizeHandleExpandSize: 5,
  areaMinSize: 10,
  inputClass: null,
  svgClass: null,
  baseHref: null,

  interactEnabled: true, // if false, disable all the below section of settings.
  shortcutsEnabled: true,
  dragNodeEnabled: true,
  selectLinkEnabled: true,
  dragNoteEnabled: true,
  dragNoteArrowEnabled: true,
  dragAreaEnabled: true,
  resizeAreaEnabled: true,
  guideLineEnabled: true,
  centerEnabled: true,
  wheelZoomEnabled: true,
  autoScaleToFitCanvas: false,

  hoverEnabled: true,
  dragCanvasEnabled: true,

  capacityEnabled: true,
  capacityRadius: 32,
  capacityVisibility: false,

  labelEnabled: true,
  labelVisibility: true,

  scaleRange: [0.4, 3],

  // D3 force options
  size: [600, 400], // D3 default: [1, 1]
  linkStrength: 1,
  // friction: 0.9,
  alpha: 1,
  alphaMin: 0.1,
  alphaDecay: 1 - Math.pow(0.001, 1 / 300),
  linkDistance: 200, // D3 default: 20
  charge: -6000,     // D3 default: -30
  // gravity: 0.1,
  theta: 0.8
};

const STATE_INITIALIZING = 'initializing';
const STATE_FORCE_TICKING = 'force-ticking';
const STATE_IDLE = 'idle';
const STATE_LINKING = 'linking';
const STATE_DBLCLICK = 'dblclick';
const STATE_DRAG_START = 'drag-start';
const STATE_DRAG_END = 'drag-end';
const STATE_DRAGGING_CANVAS = 'dragging-canvas';
const STATE_DRAG_NODE_START = 'dragging-node-start';
const STATE_DRAGGING_NODE = 'dragging-node';
const STATE_DRAG_NODE_END = 'dragging-node-end';
const STATE_DRAWING_AREA = 'drawing-area';
const STATE_DRAG_AREA_START = 'dragging-area-start';
const STATE_DRAGGING_AREA = 'dragging-area';
const STATE_DRAG_AREA_END = 'dragging-area-end';
const STATE_RESIZING_AREA = 'resizing-area';
const STATE_LOCATING_NOTE = 'locating-note';
const STATE_INPUTTING_NOTE = 'inputting-note';
const STATE_DRAG_NOTE_START = 'dragging-note-start';
const STATE_DRAGGING_NOTE = 'dragging-note';
const STATE_DRAG_NOTE_END = 'dragging-note-end';
const STATE_DRAGGING_NOTE_ARROW = 'dragging-note-arrow';
const STATE_ZOOM_CANVAS_START = 'zoom-canvas-start';
const STATE_ZOOMING_CANVAS = 'zooming-canvas';
const STATE_ZOOM_CANVAS_END = 'zoom-canvas-end';
const DEFAULT_NODE_TEXT_POSITION = nodeShapes.default.DEFAULT_TEXT_POSITION;

/**
 * Create an editable topology diagram.
 *
 * @author Wang Shenwei
 */
export default class AppsTopology extends EventEmitter {

  static STATE_INITIALIZING = STATE_INITIALIZING;
  static STATE_FORCE_TICKING = STATE_FORCE_TICKING;
  static STATE_IDLE = STATE_IDLE;
  static STATE_LINKING = STATE_LINKING;
  static STATE_DBLCLICK = STATE_DBLCLICK;
  static STATE_DRAG_START = STATE_DRAG_START;
  static STATE_DRAG_END = STATE_DRAG_END;
  static STATE_DRAGGING_CANVAS = STATE_DRAGGING_CANVAS;
  static STATE_DRAG_NODE_START = STATE_DRAG_NODE_START;
  static STATE_DRAGGING_NODE = STATE_DRAGGING_NODE;
  static STATE_DRAG_NODE_END = STATE_DRAG_NODE_END;
  static STATE_DRAWING_AREA = STATE_DRAWING_AREA;
  static STATE_DRAG_AREA_START = STATE_DRAG_AREA_START;
  static STATE_DRAGGING_AREA = STATE_DRAGGING_AREA;
  static STATE_DRAG_AREA_END = STATE_DRAG_AREA_END;
  static STATE_RESIZING_AREA = STATE_RESIZING_AREA;
  static STATE_LOCATING_NOTE = STATE_LOCATING_NOTE;
  static STATE_INPUTTING_NOTE = STATE_INPUTTING_NOTE;
  static STATE_DRAG_NOTE_START = STATE_DRAG_NOTE_START;
  static STATE_DRAGGING_NOTE = STATE_DRAGGING_NOTE;
  static STATE_DRAG_NOTE_END = STATE_DRAG_NOTE_END;
  static STATE_DRAGGING_NOTE_ARROW = STATE_DRAGGING_NOTE_ARROW;
  static STATE_ZOOM_CANVAS_START = STATE_ZOOM_CANVAS_START;
  static STATE_ZOOMING_CANVAS = STATE_ZOOMING_CANVAS;
  static STATE_ZOOM_CANVAS_END = STATE_ZOOM_CANVAS_END;
  static DEFAULT_NODE_TEXT_POSITION = DEFAULT_NODE_TEXT_POSITION;

  /**
   * @constructor
   * @param {Object} data Data of the topology `{nodes, links, areas, notes, fixed}`.
   * @param {Object} options The options transfer to d3 force layout.
   */
  constructor({nodes = [], links = [], areas = [], notes = [], fixed = false} = {}, options = {}) {
    super();

    this.nodes = nodes;
    this.links = links;
    this.areas = areas;
    this.notes = notes;
    this.options = _.defaults(options, DEFAULT_OPTIONS);
    this.history = new History({maxStates: options.historyMaxStates});
    this.keyId = options.keyId;
    this.nodeFields = options.nodeFields;
    this.linkFields = options.linkFields;
    this._window = d3.select(window);
    this.state = STATE_INITIALIZING;
    this.uniqId = _.uniqueId('topo');

    if (!options.interactEnabled) {
      _.forEach([
        'shortcutsEnabled',
        'dragNodeEnabled',
        'selectLinkEnabled',
        'dragNoteEnabled',
        'dragNoteArrowEnabled',
        'dragAreaEnabled',
        'resizeAreaEnabled',
        'guideLineEnabled'
      ], function(opt){
        options[opt] = false;
      });
    }

    if (!options.initialized) {
      // Links should be initialized for d3 force layout
      this.initLinks();
    }

    _.forEach(areas, area => {
      area.areaId = _.uniqueId('area');
    });
    _.forEach(notes, note => {
      note.noteId = _.uniqueId('note');
    });

    this.fixed = fixed;

    // Force layout only if diagram were not fixed.
    if (!fixed) {
      let {size, linkStrength, alpha, alphaMin, linkDistance, charge, theta, keyId} = options;

      this._force = d3.forceSimulation()
          .alpha(alpha)
          .alphaMin(alphaMin)
          .force('link', d3.forceLink()
              .id(function(d) { return d[keyId]; })
              .strength(linkStrength)
              .distance(linkDistance)
          )
          .force('charge', d3.forceManyBody()
              .strength(charge)
              .theta(theta)
              .distanceMin(10)
              .distanceMax(200)
          )
          .force('center', d3.forceCenter(size[0] / 2, size[1] / 2))
          // .on('tick', () => this._tick())
          .on('end', () => this._forceEnd());
    }

    this.elements = {};
    this.elements.scaleLevel = 1;
    let root = this.elements.root = d3.select(options.appendTo);

    // The container should have a non-static position, for html elements above the svg.
    let rootStyle = getComputedStyle(root.node());
    if (rootStyle.position === 'static') {
      root.style('position', 'relative');
    }

    let svg = this.elements.svg = root.append('svg')
        .attr('class', 'apps-topology');
    if (fixed) {
      svg.classed('fixed', true);
      this.state = STATE_IDLE;
    } else {
      this.state = STATE_FORCE_TICKING;
    }
    if (options.svgClass) {
      svg.classed(options.svgClass, true);
    }
    this.setSize(options.size);

    this._initDefs();

    let canvasWrapper = this.elements.canvasWrapper = svg.append('g')
      .attr('class', 'canvas-wrapper');
    let canvasRect = this.elements.canvasRect = canvasWrapper.append('rect')
      .attr('width', this.options.size[0])
      .attr('height', this.options.size[1])
      .attr('class', 'canvas-rect')
      .style('fill', 'none')
      .style('pointer-events', 'all');

    let screenCanvas = this.elements.screenCanvas = canvasWrapper.append('g')
        .attr('class', 'screen-canvas');

    // this._initCanvasDrag();

    if (options.wheelZoomEnabled) {
      this._initCanvasWheelZoom();
    }

    let html = this.elements.html = root.append('div')
        .attr('class', 'apps-topology-html')
        .style('top', rootStyle.paddingTop)
        .style('right', rootStyle.paddingRight)
        .style('bottom', rootStyle.paddingBottom)
        .style('left', rootStyle.paddingLeft);
    let htmlInput = this.elements.htmlInput = html.append('textarea')
        .attr('class', 'input-text');
    if (options.inputClass) {
      htmlInput.classed(options.inputClass, true);
    }

    if (fixed && options.centerEnabled) {
      this.centerCanvas();
    }

    if (options.autoScaleToFitCanvas) {
      this.scaleToFitCanvas();
    }

    this.elements.area = screenCanvas.append('g')
        .attr('class', 'area-list')
        .selectAll('.area');
    this.elements.link = screenCanvas.append('g')
        .attr('class', 'link-list')
        .selectAll('.link');
    this.elements.node = screenCanvas.append('g')
        .attr('class', 'node-list')
        .selectAll('.node');
    this.elements.note = screenCanvas.append('g')
        .attr('class', 'note-list')
        .selectAll('.note');
    if (options.guideLineEnabled) {
      this.elements.guide = screenCanvas.append('g')
          .attr('class', 'guide-list')
          .selectAll('.guide');
    }
    let userCanvas = this.elements.userCanvas = screenCanvas.append('g')
        .attr('class', 'user-canvas');
    this.elements.linkLine = userCanvas.append('line')
          .attr('marker-end', this._getRelativeUrl('#link-arrow'));
    this.elements.selectRect = userCanvas.append('rect');

    this._initDrags();

    this.reflow();

    if (options.shortcutsEnabled) {
      this.enableShortcuts();
    }

    setTimeout(() => this.emit('stateChange', this.state));
  }

  /**
   * Initialize svg defs, such as <marker>, etc.
   */
  _initDefs() {
    let defs = this.elements.defs = this.elements.svg.append('defs');
    let markerAttrs = {
          viewBox: '0 0 10 10',
          refX: 7.5,
          refY: 5,
          markerWidth: 6,
          markerHeight: 6,
          orient: 'auto'
        },
        markerPath = 'M 0 0 L 10 5 L 0 10 z';

    _.forEach([{
      id: 'link-arrow',
      class: 'marker'
    },
    {
      id: 'link-arrow-hover',
      class: 'marker hover'
    },
    {
      id: 'link-arrow-active',
      class: 'marker active'
    },
    {
      id: 'link-arrow-success',
      class: 'marker success'
    },
    {
      id: 'link-arrow-warn',
      class: 'marker warn'
    },
    {
      id: 'link-arrow-danger',
      class: 'marker danger'
    }], function(attrs){
      let marker = defs.append('marker');
      _.forEach(markerAttrs, function(value, key){
        marker.attr(key, value);
      });
      _.forEach(attrs, function(value, key){
        marker.attr(key, value);
      });
      marker.append('path')
          .attr('d', markerPath);
    });
  }

  /**
   * Initialize drag canvas
   */
  _initCanvasDrag() {
    let canvasOffset = this.canvasOffset = {
      x: 0,
      y: 0
    };

    if (this.options.dragCanvasEnabled) {
      let {svg, screenCanvas} = this.elements,
          x0,
          y0,
          dx,
          dy;

      this.dragCanvas = d3.drag()
          .on('start', () => {
            d3.event.sourceEvent.stopPropagation();
            svg.classed('dragging-canvas', true);
            x0 = canvasOffset.x;
            y0 = canvasOffset.y;
            dx = dy = 0;

            this.state = STATE_DRAGGING_CANVAS;
            this.emit('stateChange', this.state);
          })
          .on('drag', () => {
            dx += d3.event.dx;
            dy += d3.event.dy;
            canvasOffset.x = x0 + dx;
            canvasOffset.y = y0 + dy;

            // screenCanvas.attr('transform', `translate(${canvasOffset.x},${canvasOffset.y})`);

            let transform = screenCanvas.attr('transform') || '',
                translateReg = /(translate)(\(.+?\))/ig,
                translate = transform.match(translateReg),
                newTranslate = `translate(${canvasOffset.x},${canvasOffset.y})`;
            if (translate) {
              screenCanvas.attr('transform', transform.replace(translateReg, newTranslate));
            } else {
              screenCanvas.attr('transform', `${transform} ${newTranslate}`);
            }
          })
          .on('end', () => {
            svg.classed('dragging-canvas', false);

            this.state = STATE_IDLE;
            this.emit('stateChange', this.state);
          });

      svg.call(this.dragCanvas);
    }
  }

  /**
   * Initialize to use mouse wheel to zoom canvas
   */
  _initCanvasWheelZoom() {
    let {svg, canvasWrapper, screenCanvas} = this.elements;
    let scaleRange = this.options.scaleRange;

    this.elements.transform = d3.zoomTransform(canvasWrapper.node());

    this.zoomCanvas = d3.zoom().scaleExtent(scaleRange)
        .on('start', () => {
          svg.classed('zooming-canvas', true);

          this.state = STATE_ZOOM_CANVAS_START;
          this.emit('stateChange', this.state);
        })
        .on('start.mousedown', () => {
          svg.classed('dragging-canvas', true);
        })
        .on('zoom', () => {
          let transform = this.elements.transform = d3.zoomTransform(canvasWrapper.node());
          let scaleLevel = this.elements.scaleLevel = transform.k;

          if (scaleLevel < 0.8) {
            this._setLabelVisibility(false);
          } else {
            this._setLabelVisibility(true);
          }

          if (scaleLevel < 0.55) {
            this._setNoteVisibility(false);
          } else {
            this._setNoteVisibility(true);
          }

          screenCanvas.attr('transform',
            `translate(${transform.x},${transform.y})scale(${transform.k})`
          );

          this.state = STATE_ZOOMING_CANVAS;
          this.emit('stateChange', this.state);
        })
        .on('end', () => {
          svg.classed('zooming-canvas dragging-canvas', false);

          this.state = STATE_ZOOM_CANVAS_END;
          this.emit('stateChange', this.state);
          this.state = STATE_IDLE;
          this.emit('stateChange', this.state);
        });

    canvasWrapper.call(this.zoomCanvas)
        .on('dblclick.zoom', () => {
          this.state = STATE_DBLCLICK;
          this.emit('stateChange', this.state);
        });
  }

  /**
   * Get hoved node
   *
   * @returns {Object|Boolean} node|false
   */
  getHovedNode() {
    if (!d3.event || !d3.event.target) {
      return false;
    }

    return this.getClosestNode(d3.event.target) || false;
  }

  /**
   * Get canvas scale range
   *
   * @return {Array} [scaleMin, scaleMax]
   */
  getScaleRange() {
    let {scaleRange} = this.options;
    return scaleRange;
  }

  /**
   * Get canvas scale level
   *
   * @return {Number} canvas scale level
   */
  getScaleLevel() {
    let {scaleLevel} = this.elements;
    return scaleLevel;
  }

  /**
   * Get canvas transform value from d3
   *
   * @return {Object} canvas transform {x, y, k}
   */
  getCanvasTransform() {
    let {transform} = this.elements;
    return transform;
  }

  /**
   * Set canvas scale level
   *
   * @param {Number} canvas scale level
   */
  setCanvasScale(scaleLevel, duration = 300) {
    if (isNaN(scaleLevel)) throw new TypeError('scaleLevel should be a number.');

    let {zoomCanvas} = this,
        {canvasWrapper} = this.elements;

    canvasWrapper.transition().duration(duration).call(zoomCanvas.scaleTo, scaleLevel);
  }

  setCanvasRectSize([width, height]) {
    let {canvasRect} = this.elements;
    if (!canvasRect) return;

    width = width || canvasRect.attr('width');
    height = height || canvasRect.attr('height');

    canvasRect
        .attr('width', width)
        .attr('height', height);
  }

  _setLabelVisibility(visible = true) {
    if (this.elements.labelVisibility === visible) return;

    const labelHiddenClassName = 'hidden';
    let {screenCanvas} = this.elements;
    this.elements.labelVisibility = visible;

    screenCanvas
        .selectAll('g.node-label')
        .classed(labelHiddenClassName, !visible);
  }

  _setNoteVisibility(visible = true) {
    if (this.elements.nodeTextVisibility === visible) return;

    const nodeTextHiddenClassName = 'hidden';
    let {screenCanvas} = this.elements;
    this.elements.nodeTextVisibility = visible;

    screenCanvas
        .selectAll('g.note text.note-text')
        .classed(nodeTextHiddenClassName, !visible);
  }

  /**
   * Initialize node drag.
   */
  _initDrags() {
    let {guideLineEnabled, guideThreshold,} = this.options;
    let {nodes} = this;

    // Nodes can be dragged
    this.dragNode = d3.drag()
        .on('start', node => {
          d3.event.sourceEvent.stopPropagation();
          this.elements.svg.classed('dragging-node', true);
          node._beforeDragX = node.x;
          node._beforeDragY = node.y;
          node._dx = 0;
          node._dy = 0;
          this.setActive('node', node);

          this.state = STATE_DRAG_START;
          this.emit('stateChange', this.state);

          this.state = STATE_DRAG_NODE_START;
          this.emit('stateChange', this.state);
        })
        .on('drag', node => {
          node._dx += d3.event.dx;
          node._dy += d3.event.dy;
          node.x = node._beforeDragX + node._dx;
          node.y = node._beforeDragY + node._dy;

          // Show guide
          if (guideLineEnabled && nodes.length > 1) {
            let closest = this.getClosestGuides(node);
            if (closest.vertical.diff < guideThreshold) {
              node.x += closest.vertical.value;
            }
            if (closest.horizontal.diff < guideThreshold) {
              node.y += closest.horizontal.value;
            }
          }

          this.state = STATE_DRAGGING_NODE;
          this.emit('stateChange', this.state);

          this._tick({node});
        })
        .on('end', node => {
          this.elements.svg.classed('dragging-node', false);

          if (guideLineEnabled) {
            this.elements.guide.selectAll('line').classed('active', false);
          }

          let from = {
            x: node._beforeDragX,
            y: node._beforeDragY
          };
          let to = {
            x: node.x,
            y: node.y
          };

          delete node._beforeDragX;
          delete node._beforeDragY;
          delete node._dx;
          delete node._dy;

          // Push history only if node moved
          if (!_.isEqual(from, to)) {
            this._pushHistory('dragNode', {node, from, to});
          }

          this.state = STATE_DRAG_END;
          this.emit('stateChange', this.state);

          this.state = STATE_IDLE;
          this.emit('stateChange', this.state);
        });

    this.dragArea = d3.drag()
        .on('start', area => {
          d3.event.sourceEvent.stopPropagation();
          this.elements.svg.classed('dragging-area', true);
          area._beforeDragX = area.x;
          area._beforeDragY = area.y;
          this.setActive('area', area);

          this.state = STATE_DRAG_START;
          this.emit('stateChange', this.state);

          this.state = STATE_DRAG_AREA_START;
          this.emit('stateChange', this.state);
        })
        .on('drag', area => {
          area.x += d3.event.dx;
          area.y += d3.event.dy;
          // d3.select(this).attr('transform', `translate(${area.x},${area.y})`);

          this.state = STATE_DRAGGING_AREA;
          this.emit('stateChange', this.state);

          this._tick({});
        })
        .on('end', area => {
          this.elements.svg.classed('dragging-area', false);

          let from = {
            x: area._beforeDragX,
            y: area._beforeDragY
          };
          let to = {
            x: area.x,
            y: area.y
          };

          delete area._beforeDragX;
          delete area._beforeDragY;

          // Push history only if area moved
          if (!_.isEqual(from, to)) {
            this._pushHistory('dragArea', {area, from, to});
          }

          this.state = STATE_DRAG_END;
          this.emit('stateChange', this.state);

          this.state = STATE_IDLE;
          this.emit('stateChange', this.state);
        });

    this.dragNote = d3.drag()
        .on('start', note => {
          d3.event.sourceEvent.stopPropagation();
          this.elements.svg.classed('dragging-note', true);
          note._beforeDragX = note.x;
          note._beforeDragY = note.y;
          this.setActive('note', note);

          this.state = STATE_DRAG_START;
          this.emit('stateChange', this.state);

          this.state = STATE_DRAG_NOTE_START;
          this.emit('stateChange', this.state);
        })
        .on('drag', note => {
          note.x += d3.event.dx;
          note.y += d3.event.dy;
          // d3.select(this).attr('transform', `translate(${note.x},${note.y})`);

          this.state = STATE_DRAGGING_NOTE;
          this.emit('stateChange', this.state);

          this._tick({});
        })
        .on('end', note => {
          this.elements.svg.classed('dragging-note', false);

          let from = {
            x: note._beforeDragX,
            y: note._beforeDragY
          };
          let to = {
            x: note.x,
            y: note.y
          };

          delete note._beforeDragX;
          delete note._beforeDragY;

          // Push history only if note moved
          if (!_.isEqual(from, to)) {
            this._pushHistory('dragNote', {note, from, to});
          }

          this.state = STATE_DRAG_END;
          this.emit('stateChange', this.state);

          this.state = STATE_IDLE;
          this.emit('stateChange', this.state);
        });
  }

  /**
   * to center the canvas inside the svg.
   */
  centerCanvas() {
    if (!this.options.centerEnabled) return;

    const svgWidth = this.options.size[0],
        svgHeight = this.options.size[1];

    let xs = _.map(this.nodes, 'x').concat(_.map(this.areas, 'x')).concat(_.map(this.notes, 'x')),
        ys = _.map(this.nodes, 'y').concat(_.map(this.areas, 'y')).concat(_.map(this.notes, 'y'));

    let xMin = _.min(xs),
        xMax = _.max(xs),
        yMin = _.min(ys),
        yMax = _.max(ys),
        canvasWidth = xMax - xMin,
        canvasHeight = yMax - yMin;

    const xInterval = 150,
          yInterval = 100;

    let offsetX = 0,
        offsetY = 0;

    offsetX = xMin - (xMin + (svgWidth - xMax)) / 2;
    offsetY = yMin - (yMin + (svgHeight - yMax)) / 2;

    this.nodes = _.map(this.nodes, (node) => {
      // node.x -= (canvasWidth <= width) ? offsetX : (xMin - xInterval);
      // node.y -= (canvasHeight <= height) ? offsetY : (yMin - yInterval);
      node.x -= offsetX;
      node.y -= offsetY;
      return node;
    });
    this.areas = _.map(this.areas, (area) => {
      // area.x -= (canvasWidth <= svgWidth) ? offsetX : (xMin - xInterval);
      // area.y -= (canvasHeight <= svgHeight) ? offsetY : (yMin - yInterval);
      area.x -= offsetX;
      area.y -= offsetY;
      return area;
    });
    this.notes = _.map(this.notes, (note) => {
      // note.x -= (canvasWidth <= svgWidth) ? offsetX : (xMin - xInterval);
      // note.y -= (canvasHeight <= svgHeight) ? offsetY : (yMin - yInterval);
      note.x -= offsetX;
      note.y -= offsetY;
      return note;
    });
  }

  scaleToFitCanvas() {
    if (!this.options.autoScaleToFitCanvas) return;

    const xInterval = 150,
          yInterval = 100;

    let xs = _.map(this.nodes, 'x').concat(_.map(this.areas, 'x')).concat(_.map(this.notes, 'x')),
        ys = _.map(this.nodes, 'y').concat(_.map(this.areas, 'y')).concat(_.map(this.notes, 'y'));

    let xMin = _.min(xs),
        xMax = _.max(xs),
        yMin = _.min(ys),
        yMax = _.max(ys),
        canvasWidth = xMax - xMin + xInterval,
        canvasHeight = yMax - yMin + yInterval,
        svgWidth = this.options.size[0],
        svgHeight = this.options.size[1];

    let widthOverflow = canvasWidth - svgWidth,
        heightOverflow = canvasHeight - svgHeight;

    let scaleRate = 1;

    if (widthOverflow > 0 || heightOverflow > 0) {
      if (widthOverflow > heightOverflow) {
        scaleRate = svgWidth / canvasWidth;
      } else {
        scaleRate = svgHeight / canvasHeight;
      }
    }

    this.setCanvasScale(scaleRate);
  }

  /**
   * get minimum width of canvas
   *
   * @return {Number} minimum width of canvas
   */
  getMinWidth() {
    let xs = _.map(this.nodes, 'x')
      .concat(_.map(this.areas, 'x'))
      .concat(_.map(this.notes, 'x'));

    let xMin = _.min(xs),
      xMax = _.max(xs);

    return xMax - xMin;
  }

  /**
   * get minimum height of canvas
   *
   * @return {Number} minimum height of canvas
   */
  getMinHeight() {
    let ys = _.map(this.nodes, 'y')
      .concat(_.map(this.areas, 'y'))
      .concat(_.map(this.notes, 'y'));

    let yMin = _.min(ys),
        yMax = _.max(ys);

    return yMax - yMin;
  }

  /**
   * Should remove listeners when the topo destroyed.
   */
  destroy() {
    this._window.on(`.${this.uniqId}`, null);
    Mousetrap.reset();
  }

  /**
   * Bind some keyboard shortcuts.
   */
  enableShortcuts() {
    Mousetrap.bind('mod+z', () => {
      this.undo();
      return false;
    });
    Mousetrap.bind('mod+shift+z', () => {
      this.redo();
      return false;
    });
    Mousetrap.bind(['del', 'backspace'], () => {
      let keepDefault = !this._activeElem;
      this.removeNode();
      this.unlink();
      this.removeArea();
      this.removeNote();
      return keepDefault;
    });
    Mousetrap.bind('esc', () => this.idle());
  }

  /**
   * Get topology data to be stored.
   *
   * @returns {Object} data `{nodes, links}`
   */
  getData() {
    const keyId = this.keyId;
    // Pick fields to be stored.
    const nodeFields = _.concat(this.nodeFields, ['x', 'y', 'shape', 'label', 'transform', 'textPosition', keyId]);
    const linkFields = this.linkFields;
    const areaFields = ['x', 'y', 'width', 'height'];
    const noteFields = ['x', 'y', 'content', 'arrowDirection'];
    let nodes = _.map(this.nodes, d => _.pick(d, nodeFields));
    _.forEach(nodes, node => {
      node.shape = node.shape.shapeName;
    });

    return {
      nodes,
      links: _.map(this.links, function(d){
        let link = _.pick(d, linkFields);
        // Store nodes's keyId as `link.source` or `link.target`.
        link.source = d.source[keyId];
        link.target = d.target[keyId];
        return link;
      }),
      areas: _.map(this.areas, d => _.pick(d, areaFields)),
      notes: _.map(this.notes, d => _.pick(d, noteFields)),
      fixed: this.fixed
    };
  }

  /**
   * Set canvas size.
   *
   * @param {Array} size `[width, height]`
   */
  setSize([width, height]) {
    this.elements.svg
        .attr('width', width)
        .attr('height', height);
    this.setCanvasRectSize([width, height]);
  }

  /**
   * Reflow the diagram when nodes or links changed (appended/removed/unpdated).
   * Emit the `reflow` event, too.
   *
   * @param {boolean} reorder Does it need reorder? Defaults to false.
   */
  reflow(reorder = false, target = null) {
    if (!target || target.node) {
      this._reflowNodes();
      if (this.options.guideLineEnabled) {
        this._reflowGuides();
      }
    }

    if (!target || target.link || target.node) {
      this._reflowLinks();
    }

    if (!target || target.area) {
      this._reflowAreas();
    }

    if (!target || target.note) {
      this._reflowNotes();
    }

    if (reorder) {
      this.elements.node.order();
      // this.elements.area.order();
    }

    // Start ticking.
    if (this._force) {
      this._force.nodes(this.nodes)
          .force('link').links(this.links);
      // this._force.restart();
    } else {
      // Just tick when reflowing after force layout ended.
      this._tick(target);
    }

    // Emit event a little bit later.
    setTimeout(() => this.emit('reflow'));
  }

  /**
   * Reflow the nodes.
   */
  _reflowNodes() {
    const keyId = this.keyId;

    let node = this.elements.node.data(this.nodes, d => d[keyId]);

    // Exits.
    node.exit().remove();

    // Enters.
    let nodeEnter = node.enter()
        .append('g')
        .attr('class', 'node')
        .attr('data-app-id', d => d.appId);

    node = this.elements.node = nodeEnter.merge(node);

    let topo = this;
    nodeEnter.each(function(nd){
      let elem = d3.select(this);
      if (typeof nd.shape === 'string') {
        nd.shape = new nodeShapes[nd.shape](nd, topo);
      }
      nd.shape.renderTo(elem);
    });

    nodeEnter.on('click', () => {
      d3.event.stopPropagation();
      // this.setActive('node', node);
    });

    let {hoverEnabled} = this.options;
    nodeEnter.select('.node-icon').on('mouseenter', function(nd){
      topo.emit('nodeMouseenter', nd);

      if (topo.state !== STATE_IDLE) {
        return;
      }

      if (hoverEnabled) {
        node.classed('hover-active', false)
            .classed('hover-inactive', true);
        d3.select(this.parentNode).classed('hover-active', true)
            .classed('hover-inactive', false);

        topo.elements.link.each(function(ln){
          let related;
          if (ln.source === nd) {
            related = ln.target;
          } else if (ln.target === nd) {
            related = ln.source;
          }
          d3.select(this).classed('hover-active-related', !!related)
              .classed('hover-inactive', !related);
          if (related) {
            node.each(function(d){
              if (related === d) {
                d3.select(this).classed('hover-inactive', false);
                d3.select(this).classed('hover-active-related', true);
              }
            });
          }
        });

        topo.elements.area.classed('hover-inactive', true);
        topo.elements.note.classed('hover-inactive', true);
      }
    });

    nodeEnter.select('.node-icon').on('mouseleave', nd => {
      this.emit('nodeMouseleave', nd);

      if (topo.state !== STATE_IDLE) {
        return;
      }

      if (hoverEnabled) {
        node.classed('hover-active', false)
            .classed('hover-inactive', false)
            .classed('hover-active-related', false);

        topo.elements.link.classed('hover-active-related', false)
            .classed('hover-inactive', false);

        topo.elements.area.classed('hover-inactive', false);
        topo.elements.note.classed('hover-inactive', false);
      }
    });

    /*nodeEnter.on('mousedown', function(){
      // d3.event.preventDefault();
      // d3.event.stopPropagation();
      d3.event.stopPropagation();
    });*/

    if (!this._force) {
      // Disable drag before force layout ended.
      if (this.options.dragNodeEnabled) {
        nodeEnter.call(this.dragNode);
      }
    }
  }

  /**
   * Reflow the guide lines.
   */
  _reflowGuides() {
    const keyId = this.keyId;

    let guide = this.elements.guide.data(this.nodes, d => d[keyId]);

    // Exits.
    guide.exit().remove();

    // Enters.
    let guideEnter = guide.enter()
        .append('g')
        .attr('class', 'guide');

    this.elements.guide = guideEnter.merge(guide);

    _.forEach(['cx', 'left', 'right', 'cy', 'top', 'bottom'], function(pos){
      guideEnter.append('line')
          .attr('class', `pos-${pos}`);
    });
  }

  /**
   * Reflow the links.
   */
  _reflowLinks() {
    const keyId = this.keyId;

    let link = this.elements.link.data(this.links, d => `${d.source[keyId]}=>${d.target[keyId]}`);

    // Exits.
    link.exit().remove().each(link => {
      if (link.mirror) {
        delete link.mirror.mirror;
        delete link.mirror;
      }
    });

    // Enters.
    let linkEnter = link.enter()
        .append('g')
        .attr('class', 'link')
        .attr('data-link-ids', d => `${d.source.appId}-${d.target.appId}`);
    linkEnter
        .append('line')
        .attr('class', 'link-bg');
    linkEnter
        .append('line')
        .attr('class', 'link-arrow')
        .attr('marker-end', this._getRelativeUrl('#link-arrow'));
    linkEnter.append('text')
        .attr('dy', 4.5)
        .attr('class', 'link-text')
        .text(d => `${d.source.name[0]} --> ${d.target.name[0]}`);

    this.elements.link = linkEnter.merge(link);

    let topo = this,
        storedMarker = '';
    linkEnter.on('mouseenter', function(d, i, n){
      let elem = d3.select(this);
      if (elem.classed('active')) {
        return;
      }
      storedMarker = elem.select('.link-arrow').attr('marker-end');
      elem.select('.link-arrow').attr('marker-end', topo._getRelativeUrl('#link-arrow-hover'));
    }).on('mouseleave', function(){
      let elem = d3.select(this);
      if (elem.classed('active')) {
        return;
      }
      // elem.select('.link-arrow').attr('marker-end', topo._getRelativeUrl('#link-arrow'));
      elem.select('.link-arrow').attr('marker-end', storedMarker);
    });

    // Should prevent text selection.
    linkEnter.on('mousedown', () => d3.event.preventDefault());

    if (this.options.selectLinkEnabled) {
      linkEnter.on('click', link => {
        d3.event.stopPropagation();
        this.setActive('link', link);
      });
    }

    linkEnter.each(link => {
      if (link.mirror) {
        return;
      }
      let mirror = _.find(this.links, d => d.source === link.target && d.target === link.source);
      if (mirror) {
        link.mirror = mirror;
        mirror.mirror = link;
      }
    });
  }

  /**
   * Show link text
   *
   * @param {Boolean} to show or hide link text
   */

  showLinkText(show = true) {
    let {svg} = this.elements;

    svg.classed('show-link-text', show);

    if (!show) {
      let links = svg.selectAll('g.link');
      links
        .selectAll('text.link-text')
        .text('');
      links
        .selectAll('line.link-arrow')
        .attr('marker-end', this._getRelativeUrl('#link-arrow'))
        .classed('success', false)
        .classed('warn', false)
        .classed('danger', false);
    }
  }

  _linkColorScale(rate) {
    if (isNaN(rate)) throw new TypeError('Argument should be number.');
    if (rate < 0 || rate > 1) throw new RangeError('Argument should whithin the range 0 to 1.');

    let color = '';
    if (rate <= 0.2) {
      color = '#ff0000';
    } else if (rate > 0.2 && rate <= 0.4) {
      color = 'yellow';
    } else {
      color = '#18DB02';
    }
    return color;
  }

  _linkColorScaleArrow(rate) {
    if (isNaN(rate)) throw new TypeError('Argument should be number.');
    if (rate < 0 || rate > 1) throw new RangeError('Argument should whithin the range 0 to 1.');

    let markerEnd = null;
    if (rate < 0.1) {
      markerEnd = this._getRelativeUrl('#link-arrow-danger');
    } else if (rate >= 0.1 && rate < 0.6) {
      markerEnd = this._getRelativeUrl('#link-arrow-warn');
    } else {
      markerEnd = this._getRelativeUrl('#link-arrow-success');
    }
    return markerEnd;
  }

  /**
   * Set all links text with an array
   *
   * @param {Array} [{source, target, text}] links to be set text
   */
  setLinksText(links, unit = '', stateColor = false) {
    if (!links) {
      return;
    }
    if (!_.isArray(links)) {
      throw new TypeError('the argument should be Array');
    }

    let {screenCanvas} = this.elements;

    _.forEach(links, link => {
      let elem = screenCanvas
        .select(`g.link[data-link-ids="${link.source}-${link.target}"]`);

      elem
        .select('text.link-text')
        .text(d => {
          let text = `${link.text}${unit}`;
          return text;
        });

      if (stateColor) {
        let rate = link.text / 100;
        elem
          .select('line.link-arrow')
          .attr('marker-end', d => {
            let rate = link.text / 100;
            if (!isNaN(rate)) {
              return this._linkColorScaleArrow(rate);
            }
          })
          .classed('success', rate >= 0.6)
          .classed('warn', rate < 0.6 && rate >= 0.1)
          .classed('danger', rate < 0.1);
      }
    });
  }

  /**
   * Reflow the areas.
   */
  _reflowAreas() {
    let area = this.elements.area.data(this.areas, d => d.areaId);

    // Exits.
    area.exit().remove();

    // Enters.
    let areaEnter = area.enter()
        .append('g')
        .attr('class', 'area')
        .attr('transform', d => `translate(${d.x},${d.y})`);
    areaEnter
        .append('rect')
        .attr('class', 'area-bg')
        .attr('x', 0)
        .attr('y', 0)
        .attr('width', 0)
        .attr('height', 0);

    this.elements.area = areaEnter.merge(area);

    if (this.options.resizeAreaEnabled) {
      let directions = [
        'west',
        'north',
        'east',
        'south',
        'south-west',
        'north-west',
        'north-east',
        'south-east'
      ];
      let {_window, uniqId} = this,
          areaMinSize = this.options.areaMinSize,
          svg = this.elements.svg,
          topo = this;
      _.forEach(directions, function(direction){
        areaEnter
          .append('rect')
          .attr('class', `area-handle area-handle-${direction}`)
          .attr('x', 0)
          .attr('y', 0)
          .attr('width', 0)
          .attr('height', 0)
          .on('mousedown', function(area){
            // Should prevent the default behaviour.
            d3.event.preventDefault();
            d3.event.stopPropagation();

            // let [x1, y1] = d3.mouse($svg);
            let x1 = d3.event.x,
                y1 = d3.event.y,
                from = _.pick(area, ['x', 'y', 'width', 'height']),
                to = _.clone(from);

            svg.classed('resizing-area', true);

            _window
                .on(`mousemove.${uniqId}`, function(){
                  let x2 = d3.event.x,
                      y2 = d3.event.y,
                      dx = Math.round(x2 - x1),
                      dy = Math.round(y2 - y1);

                  if (direction.indexOf('east') !== -1) {
                    to.width = from.width + dx;
                  } else if (direction.indexOf('west') !== -1) {
                    to.x = from.x + dx;
                    to.width = from.width - dx;
                  }
                  if (direction.indexOf('south') !== -1) {
                    to.height = from.height + dy;
                  } else if (direction.indexOf('north') !== -1) {
                    to.y = from.y + dy;
                    to.height = from.height - dy;
                  }

                  if (to.width < areaMinSize) {
                    let width = to.width;
                    to.width = areaMinSize;
                    if (direction.indexOf('west') !== -1) {
                      to.x -= areaMinSize - width;
                    }
                  }
                  if (to.height < areaMinSize) {
                    let height = to.height;
                    to.height = areaMinSize;
                    if (direction.indexOf('north') !== -1) {
                      to.y -= areaMinSize - height;
                    }
                  }

                  _.assign(area, to);

                  topo._tick({});
                })
                .on(`mouseup.${uniqId}`, function(){
                  _window.on(`mousemove.${uniqId}`, null).on(`mouseup.${uniqId}`, null);
                  svg.classed('resizing-area', false);

                  // Push history only if area resized
                  if (!_.isEqual(from, to)) {
                    topo._pushHistory('resizeArea', {area, from, to});
                  }

                  topo.state = STATE_IDLE;
                  topo.emit('stateChange', topo.state);
                });

            topo.state = STATE_RESIZING_AREA;
            topo.emit('stateChange', topo.state);
          });
      });
    }

    areaEnter.on('click', () => {
      d3.event.stopPropagation();
      // this.setActive('area', area);
    });
    if (this.options.dragAreaEnabled) {
      areaEnter.call(this.dragArea);
    }
  }

  /**
   * Reflow the notes.
   */
  _reflowNotes() {
    let note = this.elements.note.data(this.notes, d => d.noteId);

    // Exits.
    note.exit().remove();

    // Enters.
    let noteEnter = note.enter()
        .append('g')
        .attr('class', 'note')
        .attr('transform', d => `translate(${d.x},${d.y})`);

    this.elements.note = noteEnter.merge(note);

    if (this.options.dragNoteEnabled) {
      noteEnter.call(this.dragNote);
    }

    let topo = this,
        svg = this.elements.svg;
    noteEnter.each(function(note){
      let elem = d3.select(this);
      if (!note.shape) {
        note.shape = new NoteShape(note, topo);
      }
      note.shape.renderTo(elem);

      if (topo.options.dragNoteArrowEnabled) {
        elem.classed('drag-note-arrow-enabled', true);
        let shape = note.shape;
        let arrowDrag = d3.drag()
            .on('start', function(){
              d3.event.sourceEvent.preventDefault();
              d3.event.sourceEvent.stopPropagation();

              note._oldArrowDirection = note.arrowDirection;
              shape._arrowDx = shape._arrowX;
              shape._arrowDy = shape._arrowY;

              svg.classed('dragging-note-arrow', true);

              topo.state = STATE_DRAGGING_NOTE_ARROW;
              topo.emit('stateChange', topo.state);
            })
            .on('drag', function(){
              shape._arrowDx += d3.event.dx;
              shape._arrowDy += d3.event.dy;

              let angle = Math.atan(shape._arrowDy / shape._arrowDx);
              if (_.isNaN(angle)) {
                angle = NoteShape.DEFAULT_ARROW_DIRECTION;
              } else if (shape._arrowDx < 0) {
                angle += Math.PI;
              }
              shape.setArrowDirection(angle);
            })
            .on('end', function(){
              svg.classed('dragging-note-arrow', false);

              delete shape._arrowDx;
              delete shape._arrowDy;

              let from = note._oldArrowDirection,
                  to = note.arrowDirection;
              if (from !== to) {
                topo._pushHistory('dragNoteArrow', {note, from, to});
              }

              topo.state = STATE_IDLE;
              topo.emit('stateChange', topo.state);
            });
        elem.select('.arrow-handle').call(arrowDrag);
      }
    });

    /*noteEnter.append('text')
        .attr('class', 'note-text')
        .attr('x', 0)
        .attr('y', 0)
        .attr('dy', 6)
        .text(d => d.text);*/
    noteEnter.on('click', () => {
      d3.event.stopPropagation();
      // this.setActive('note', note);
    });
  }

  /**
   * D3 force layout tick.
   *
   * @private
   * @param {object} target The dragging target.
   */
  _tick(target = null) {
    // console.log('tick');

    let {node, link, guide, area, note} = this.elements;

    /*if (this._force) {
      this._stopEarly();
    }*/

    // If dragging a non-node, ignore nodes.
    if (!target || target.node) {
      if (target && target.node) {
        node.each(function(d){
          if (d === target.node) {
            d3.select(this).attr('transform', d.shape.getAbsoluteTransform());
          }
        });
      } else {
        node.attr('transform', d => d.shape.getAbsoluteTransform());
      }

      if (this.options.guideLineEnabled) {
        let [width, height] = this.options.size;
        guide.each(function(node){
          let elem = d3.select(this),
              shape = node.shape;
          _.forEach({vertical: shape.getVerticals(), horizontal: shape.getHorizontals()}, function(group, direction){
            _.forEach(group, function(value, pos){
              value = Math.round(value - 0.5) + 0.5;
              let line = elem.select(`.pos-${pos}`);
              if (direction === 'vertical') {
                line
                  .attr('x1', value)
                  .attr('y1', 0)
                  .attr('x2', value)
                  .attr('y2', height);
              } else {
                line
                  .attr('x1', 0)
                  .attr('y1', value)
                  .attr('x2', width)
                  .attr('y2', value);
              }
            });
          });
        });
      }
    }

    // If dragging a non-node-or-link, ignore links.
    if (!target || target.node || target.link) {
      if (target) {
        // console.log('here!', target, target.node, target.link);
      }

      // let options = _.pick(this.options, ['linkStartOffset', 'linkEndOffset']);
      let {linkMirrorGap, linkTextOffset, clearOnePxLink} = this.options;
      link.each(function(d){
        // let needLog = d.source.name.substr(0, 3) === '1so' && d.target.name.substr(0, 3) === '4g5';

        // If dragging a node, ignore irrelevant links.
        if (target && target.node) {
          if (d.source !== target.node && d.target !== target.node) {
            return;
          }
        }

        // If dragging a link, ignore irrelevant links.
        if (target && target.link) {
          if (target.link !== d && !(d.source === target.link.target && d.target === target.link.source)) {
            return;
          }
        }

        let src = d.source.shape,
            tgt = d.target.shape;

        if (src.px !== undefined && tgt.px !== undefined && src.px === src.x && src.py === src.y && tgt.px === tgt.x && tgt.py === tgt.y) {
          // Nothing changed.
          return;
        }

        let originalDx = d.target.x - d.source.x,
            originalDy = d.target.y - d.source.y,
            offsets = [0, 0],
            angle = Math.atan(originalDy / originalDx) - Math.PI / 2;
        if (_.isNaN(angle)) {
          angle = Math.PI / 2;
        } else if (originalDx < 0) {
          angle += Math.PI;
        }
        // Angle should be in range [0, Math.PI * 2];
        if (angle < 0) {
          angle += Math.PI * 2;
        }

        // If the link had a mirror, it should offset.
        if (d.mirror) {
          offsets[0] = Math.cos(angle) * linkMirrorGap / 2;
          offsets[1] = Math.sin(angle) * linkMirrorGap / 2;
        }

        // There should be gaps between links and nodes.
        let {x1, y1} = src.getLinkPointAsSource(tgt, offsets),
            {x2, y2} = tgt.getLinkPointAsTarget(src, offsets),
            dx = x2 - x1,
            dy = y2 - y1,
            points = {x1, y1, x2, y2};

        // The gaps may cause the link reversed.
        if ((dy < 0 ^ originalDy <= 0) && (dx < 0 ^ originalDx <= 0)) {
          // When link is reversed, the length may be larger than the original link's.
          let length = Math.sqrt(dy * dy + dx * dx),
              originalLength = Math.sqrt(originalDy * originalDy + originalDx * originalDx);
          if (length > originalLength) {
            // Set the link as center to center, if so.
            points = {
              x1: d.source.x + offsets[0],
              y1: d.source.y + offsets[1],
              x2: d.target.x + offsets[0],
              y2: d.target.y + offsets[1]
            };
          } else {
            // Set the link reversed back.
            points = {
              x1: x2,
              y1: y2,
              x2: x1,
              y2: y1
            };
          }
        }

        let elem = d3.select(this);
        let line = elem.selectAll('line');
        _.forEach(points, function(value, key){
          // Make a clear 1px line
          if (clearOnePxLink) {
            value = Math.round(value - 0.5) + 0.5;
          }
          line.attr(key, value);
        });
        let textAngle = (angle + Math.PI / 2) % (Math.PI * 2),
            textX = (points.x1 + points.x2) / 2 + Math.cos(angle) * linkTextOffset,
            textY = (points.y1 + points.y2) / 2 + Math.sin(angle) * linkTextOffset;
        if (textAngle > Math.PI / 2 && textAngle < Math.PI * 3 / 2) {
          textAngle += Math.PI;
        }
        elem.select('text')
          // x: (points.x1 + points.x2) / 2,
          // y: (points.y1 + points.y2) / 2,
          .attr('transform', `translate(${textX},${textY})rotate(${textAngle * 180 / Math.PI})`);
      });
    }

    // If just dragging a node, ignore areas and notes.
    if (target && target.node) {
      return;
    }

    let {resizeHandleSize, resizeHandleExpandSize} = this.options;
    let handleList = [{
      direction: 'north-west',
      x: -resizeHandleExpandSize,
      y: -resizeHandleExpandSize,
      width: resizeHandleSize + resizeHandleExpandSize,
      height: resizeHandleSize + resizeHandleExpandSize
    },{
      direction: 'north',
      x: resizeHandleSize,
      y: -resizeHandleExpandSize,
      width: d => Math.max(0, d.width - 2 * resizeHandleSize),
      height: resizeHandleSize / 2 + resizeHandleExpandSize
    },{
      direction: 'north-east',
      x: d => d.width - resizeHandleSize,
      y: -resizeHandleExpandSize,
      width: resizeHandleSize + resizeHandleExpandSize,
      height: resizeHandleSize + resizeHandleExpandSize
    },{
      direction: 'east',
      x: d => d.width - resizeHandleSize / 2,
      y: resizeHandleSize,
      width: resizeHandleSize / 2 + resizeHandleExpandSize,
      height: d => Math.max(0, d.height - 2 * resizeHandleSize)
    },{
      direction: 'south-east',
      x: d => d.width - resizeHandleSize,
      y: d => d.height - resizeHandleSize,
      width: resizeHandleSize + resizeHandleExpandSize,
      height: resizeHandleSize + resizeHandleExpandSize
    },{
      direction: 'south',
      x: resizeHandleSize,
      y: d => d.height - resizeHandleSize / 2,
      width: d => Math.max(0, d.width - 2 * resizeHandleSize),
      height: resizeHandleSize / 2 + resizeHandleExpandSize
    },{
      direction: 'south-west',
      x: -resizeHandleExpandSize,
      y: d => d.height - resizeHandleSize,
      width: resizeHandleSize + resizeHandleExpandSize,
      height: resizeHandleSize + resizeHandleExpandSize
    },{
      direction: 'west',
      x: -resizeHandleExpandSize,
      y: resizeHandleSize,
      width: resizeHandleSize / 2 + resizeHandleExpandSize,
      height: d => Math.max(0, d.height - 2 * resizeHandleSize)
    }];

    area.attr('transform', d => `translate(${d.x},${d.y})`);
    area.select('.area-bg')
        .attr('width', d => d.width)
        .attr('height', d => d.height);
    area.each(function(){
      let elem = d3.select(this);
      _.forEach(handleList, function(handle){
        elem.select(`.area-handle-${handle.direction}`)
            .attr('x', handle.x)
            .attr('y', handle.y)
            .attr('width', handle.width)
            .attr('height', handle.height);
      });
    });

    note.attr('transform', d => `translate(${d.x},${d.y})`);

    // link.attr('x1', d => d.source.x);
    // link.attr('y1', d => d.source.y);
    // link.attr('x2', d => d.target.x);
    // link.attr('y2', d => d.target.y);
  }

  /**
   * D3 force layout end.
   *
   * @private
   */
  _forceEnd() {
    // console.log('force end');

    this._tick();

    this._force.on('end', null);
    this._force = null;
    this.fixed = true;
    this.elements.svg.classed('fixed', true);

    /*let [width, height] = this.options.size;
    let fixedRect = {
      top: height,
      right: 0,
      bottom: 0,
      left: width
    };
    _.forEach(this.nodes, function(node){
      let rect = node.shape.getBoundingRect();
      // console.log(rect);
      if (rect.top < fixedRect.top) {
        fixedRect.top = rect.top;
      }
      if (rect.right > fixedRect.right) {
        fixedRect.right = rect.right;
      }
      if (rect.bottom > fixedRect.bottom) {
        fixedRect.bottom = rect.bottom;
      }
      if (rect.left < fixedRect.left) {
        fixedRect.left = rect.left;
      }
    });

    let padding = 30;
    let newWidth = fixedRect.right - fixedRect.left + 2 * padding;
    let newHeight = fixedRect.bottom - fixedRect.top + 2 * padding;
    if (newWidth > 0 && newHeight > 0) {
      let offsetX = -fixedRect.left + padding;
      let offsetY = -fixedRect.top + padding;
      this.nodes.forEach(d => {
        d.x += offsetX;
        d.y += offsetY;
      });
      this.setSize(this.options.size = [newWidth, newHeight]);
      this._tick();
    }*/

    /*_.forEach(this.nodes, function(node){
      node.fixed = true;
    });*/

    if (this.options.centerEnabled) {
      this.centerCanvas();
      this.reflow();
    }

    if (this.options.autoScaleToFitCanvas) {
      this.scaleToFitCanvas();
      this.reflow();
    }

    if (this.options.dragNodeEnabled) {
      this.elements.node.call(this.dragNode);
    }

    this.state = STATE_IDLE;
    this.emit('stateChange', this.state);

    this.emit('forceEnd');
  }

  /**
   * Get a relative url based on `<base href>`.
   *
   * @private
   * @param {string} url The original relative url.
   * @returns {string} The calculated url.
   */
  _getRelativeUrl(url) {
    let {baseHref} = this.options;
    return `url(${baseHref || ''}${url})`;
  }

  /**
   * Set the state to idle, release relevant bindings.
   */
  idle() {
    this.unsetActive();

    switch (this.state) {
      case STATE_LINKING:
        this.unToLink();
        break;
      case STATE_DRAWING_AREA:
        this.unToAppendArea();
        break;
      case STATE_LOCATING_NOTE:
      case STATE_INPUTTING_NOTE:
        this.unToAppendNote();
        break;
    }
  }

  /**
   * Initialize for appending area.
   */
  toAppendArea() {
    this.idle();

    let {svg, canvasWrapper, selectRect, screenCanvas} = this.elements,
        // $svg = svg.node(),
        $canvas = screenCanvas.node(),
        topo = this,
        {_window, uniqId} = this,
        areaMinSize = this.options.areaMinSize;

    svg.classed('drawing-area', true);

    canvasWrapper.on('.zoom', null);
    // svg.on('.drag', null);

    svg.on('mousedown', function(){
      // Should prevent the default behaviour.
      d3.event.preventDefault();

      let [x1, y1] = d3.mouse($canvas);
      x1 = Math.round(x1 - 0.5) + 0.5;
      y1 = Math.round(y1 - 0.5) + 0.5;
      let bounding = {
        x: x1,
        y: y1,
        width: 0,
        height: 0
      };
      _.forEach(bounding, function(value, key){
        selectRect.attr(key, value);
      });
      selectRect.classed('active', true);

      // Should capture the mouse events on window.
      _window
          .on(`mousemove.${uniqId}`, function(){
            let [x2, y2] = d3.mouse($canvas);
            x2 = Math.min(Math.max(0, x2), topo.options.size[0] - 1);
            y2 = Math.min(Math.max(0, y2), topo.options.size[1] - 1);
            x2 = Math.round(x2 - 0.5) + 0.5;
            y2 = Math.round(y2 - 0.5) + 0.5;
            let w = x2 - x1,
                h = y2 - y1,
                width = Math.abs(w),
                height = Math.abs(h);
            if (width < areaMinSize / 2) {
              width = 0;
            } else if (width < areaMinSize) {
              width = areaMinSize;
            }
            if (height < areaMinSize / 2) {
              height = 0;
            } else if (height < areaMinSize) {
              height = areaMinSize;
            }
            bounding = {
              x: w < 0 ? x2 : x1,
              y: h < 0 ? y2 : y1,
              width,
              height
            };
            _.forEach(bounding, function(value, key){
              selectRect.attr(key, value);
            });
          })
          .on(`mouseup.${uniqId}`, function(){
            topo.unToAppendArea();

            if (bounding.width > 0 && bounding.height > 0) {
              topo.appendArea(bounding);
            }
          });
    });

    topo.state = STATE_DRAWING_AREA;
    topo.emit('stateChange', topo.state);
  }

  /**
   * Selecting ended.
   */
  unToAppendArea() {
    let {svg, canvasWrapper, selectRect} = this.elements;
    svg.on('mousedown', null);
    this._window.on(`mousemove.${this.uniqId}`, null).on(`mouseup.${this.uniqId}`, null);
    svg.classed('drawing-area', false);
    selectRect.classed('active', false);

    // if (this.options.dragCanvasEnabled) {
    //   svg.call(this.dragCanvas);
    // }
    if (this.options.wheelZoomEnabled) {
      canvasWrapper.call(this.zoomCanvas);
    }

    this.state = STATE_IDLE;
    this.emit('stateChange', this.state);
  }

  /**
   * Initialize for appending note.
   */
  toAppendNote() {
    this.idle();

    let {html} = this.elements;

    html.classed('active locating-note', true);

    html.on('click', () => {
      d3.event.preventDefault();
      let [x, y] = d3.mouse(html.node());

      html.classed('locating-note', false);
      html.classed('editing-note', true);
      html.on('click', null);

      this.showNoteInput(x, y);
    });

    this.state = STATE_LOCATING_NOTE;
    this.emit('stateChange', this.state);
  }

  /**
   * Appending note ended.
   */
  unToAppendNote() {
    let {html, htmlInput} = this.elements;
    let $htmlInput = htmlInput.node();
    $htmlInput.value = '';
    html.on('click', null);
    html.classed('active editing-note', false);
    htmlInput.classed('active', false);
    if (this._htmlInputKeypress) {
      this._htmlInputKeypress.reset();
      delete this._htmlInputKeypress;
    }
    htmlInput.on('blur', null);

    this.state = STATE_IDLE;
    this.emit('stateChange', this.state);
  }

  /**
   * Show note input.
   *
   * @param {number} x The note's center x.
   * @param {number} y The note's center y.
   */
  showNoteInput(x, y) {
    let {html, htmlInput} = this.elements;
    let $htmlInput = htmlInput.node();
    html.on('click', null);
    htmlInput.classed('active', true);
    let bounding = $htmlInput.getBoundingClientRect();
    htmlInput
      .style('left', `${x - bounding.width / 2}px`)
      .style('top', `${y - bounding.height / 2}px`);
    $htmlInput.focus();

    // x -= this.canvasOffset.x;
    // y -= this.canvasOffset.y;

    let keypress = this._htmlInputKeypress = new Mousetrap($htmlInput);
    keypress.bind(['ctrl+enter', 'command+enter'], () => this.appendNoteAt(x, y));
    keypress.bind('esc', () => this.unToAppendNote());
    htmlInput.on('blur', () => this.appendNoteAt(x, y));

    this.state = STATE_INPUTTING_NOTE;
    this.emit('stateChange', this.state);
  }

  /**
   * Try to append note at a point.
   *
   * @param {number} x The note's center x.
   * @param {number} y The note's center y.
   */
  appendNoteAt(x, y) {
    let content = this.elements.htmlInput.node().value;

    this.unToAppendNote();

    if (content) {
      this.appendNote({content, x, y});
    }
  }

  /**
   * Set the state to STATE_LINKING, initialize for linking.
   */
  toLink() {
    this.idle();

    let {svg, canvasWrapper, linkLine, node, area, screenCanvas} = this.elements,
        // $svg = svg.node(),
        $canvas = screenCanvas.node(),
        topo = this,
        {keyId, uniqId, _window} = this;

    canvasWrapper.on('.zoom', null);
    node.on('.drag', null);
    area.on('.drag', null);

    svg.classed('linking', true)
    .on('mousedown', function(){
      // Should prevent the default behaviour.
      d3.event.preventDefault();

      let [x, y] = d3.mouse($canvas);
      linkLine
        .attr('x1', x)
        .attr('y1', y)
        .attr('x2', x)
        .attr('y2', y)
        .classed('active', true);

      // Get the closest node to he mouse.
      let source = topo.getClosestNode(d3.event.target);
      node.classed('hover', d => d === source);

      // Should capture the mouse events on window.
      _window
          .on(`mousemove.${uniqId}`, function(){
            // Get the closest node to he mouse.
            let target = topo.getClosestNode(d3.event.target);
            node.classed('hover', d => d === target || d === source);

            let [x, y] = d3.mouse($canvas);
            linkLine
              .attr('x2', x)
              .attr('y2', y);
          })
          .on(`mouseup.${uniqId}`, function(){
            topo.unToLink();

            // Get the closest node to he mouse.
            let target = topo.getClosestNode(d3.event.target);
            if (source && target && source !== target) {
              let link = {source, target},
                  linkId = `${source[keyId]}=>${target[keyId]}`;
              // Links should NOT be duplicated.
              if (!_.find(topo.links, d => linkId === `${d.source[keyId]}=>${d.target[keyId]}`)) {
                topo.link(link);
              }
            }
          });
    });

    this.state = STATE_LINKING;
    this.emit('stateChange', this.state);
  }

  /**
   * Linking ended.
   */
  unToLink() {
    let {svg, canvasWrapper, linkLine, node, area} = this.elements,
        {wheelZoomEnabled, dragNodeEnabled, dragAreaEnabled} = this.options;
    this._window.on(`mousemove.${this.uniqId}`, null).on(`mouseup.${this.uniqId}`, null);
    svg.classed('linking', false).on('mousedown', null);
    linkLine.classed('active', false);
    if (wheelZoomEnabled) {
      canvasWrapper.call(this.zoomCanvas);
    }
    if (dragNodeEnabled) {
      node.call(this.dragNode);
    }
    node.classed('hover', false);
    if (dragAreaEnabled) {
      area.call(this.dragArea);
    }

    this.state = STATE_IDLE;
    this.emit('stateChange', this.state);
  }

  /**
   * Get guide lines to assist alignment among nodes.
   *
   * @param {Object} node The node to be aligned.
   * @returns {Object} Returns the closest nodes (one for vertical, one for horizontal).
   */
  getClosestGuides(node) {
    let /*{top, left, right, bottom} = node.shape,*/
        nodes = this.nodes,
        guideThreshold = this.options.guideThreshold,
        {x: cx, y: cy} = node,
        needles = {
          vertical: {
            cx,
            // left: left + cx,
            // right: right + cx
          },
          horizontal: {
            cy,
            // top: top + cy,
            // bottom: bottom + cy
          }
        },
        closest = {};

    _.forEach(needles, function(group, direction){
      closest[direction] = _.chain(group)
        .map(function(offset, sourcePosition){
          return _.chain(nodes)
            .without(node)
            .map(function(d){
              return _.chain(d.shape[direction === 'horizontal' ? 'getHorizontals' : 'getVerticals']())
                .map(function(value, targetPosition){
                  return {
                    node: d,
                    value: value - offset,
                    diff: Math.abs(value - offset),
                    sourcePosition,
                    targetPosition,
                    direction
                  };
                }).minBy('diff')
                .value();
            }).minBy('diff')
            .value();
        }).minBy('diff')
        .value();
    });
    this.elements.guide.selectAll('line').classed('active', false);
    if (closest.vertical.diff < guideThreshold) {
      this.elements.guide.each(function(d){
        if (d === closest.vertical.node) {
          d3.select(this).select(`.pos-${closest.vertical.targetPosition}`).classed('active', true);
        }
      });
      // node.x += closest.vertical.value;
    }
    if (closest.horizontal.diff < guideThreshold) {
      this.elements.guide.each(function(d){
        if (d === closest.horizontal.node) {
          d3.select(this).select(`.pos-${closest.horizontal.targetPosition}`).classed('active', true);
        }
      });
      // node.y += closest.horizontal.value;
    }

    return closest;
  }

  /**
   * Get the closest node to the mouse event's target,
   * by traversing its ancestor elements.
   *
   * @param {HTMLElement} target The mouse event's target.
   * @returns {Object} Returns the closest node, or null if no node found.
   */
  getClosestNode(target) {
    while (target && target !== document) {
      let elem = d3.select(target);
      if (elem.classed('node')) {
        return elem.datum();
      }
      target = target.parentNode;
    }
    return null;
  }

  /*
   * Set the active datum.
   *
   * @param {string} type 'node', 'link' or 'area'.
   * @param {Object} datum
   */
  setActive(type, datum) {
    let elem = this.elements[type].filter(d => d === datum);
    if (!elem) {
      return;
    }

    // If the element is already active, do nothing.
    if (type === this._activeType && elem.datum() === (this._activeElem && this._activeElem.datum())) {
      return;
    }

    this.unsetActive(false);

    this._activeElem = elem.classed('active', true);
    this._activeType = type;
    this.elements.svg.on('click.active', () => this.unsetActive());
    if (type === 'link') {
      elem.select('.link-arrow').attr('marker-end', this._getRelativeUrl('#link-arrow-active'));
    }

    this.emit('activeChange', type, datum);
  }

  /*
   * Get the active datum.
   *
   * @param {string} type 'node', 'link' or 'area'.
   * @returns {Object} Returns the active datum.
   */
  getActive(type) {
    return this._activeType === type && this._activeElem && this._activeElem.datum();
  }

  /*
   * Unset the active datum.
   *
   * @param {boolean} emit Whether to emit an `acitveChange` event.
   */
  unsetActive(emit = true) {
    this.elements.svg.on('click.active', null);
    if (this._activeElem) {
      this._activeElem.classed('active', false);
      let type = this._activeType;
      if (type === 'link') {
        this._activeElem.select('.link-arrow').attr('marker-end', this._getRelativeUrl('#link-arrow'));
      }
      this._activeElem = null;
      this._activeType = null;
      if (emit) {
        this.emit('activeChange', type, null);
      }
    }
  }

  /**
   * Append a node.
   *
   * @param {Object} node
   * @param {number} index Which index the node should be appended at.
   * @param {boolean} notify Whether to notify to record history and reflow.
   */
  appendNode(node = {}, index = this.nodes.length, notify = true) {
    this.nodes.splice(index, 0, node);
    // console.log(`appended node ${node[this.keyId]}`);

    if (notify) {
      this._pushHistory('appendNode', {node, index});
      this.reflow(true, {node});
    }
  }

  /**
   * Remove a node.
   *
   * @param {Object} node The node to be removed, default to the active node.
   * @param {boolean} notify Whether to notify to record history and reflow.
   */
  removeNode(node, notify = true) {
    let activeNode = this.getActive('node');
    node = node || activeNode;

    if (!node) {
      return;
    }

    let index = _.findIndex(this.nodes, node);
    if (index === -1) {
      return;
    }

    _.remove(this.nodes, node);
    // console.log(`removed node ${node[this.keyId]}`);
    // Remove related links, too
    let links = _.remove(this.links, item => item.source === node || item.target === node);
    // _.forEach(links, link => {
    //   console.log(`removed node related link ${link.source[this.keyId]} => ${link.target[this.keyId]}`);
    // });
    // console.log('removed node related links', links);

    if (notify) {
      this._pushHistory('removeNode', {node, links, index});
      this.reflow(false, {node});
    }

    if (node === activeNode) {
      this.unsetActive();
    }
  }

  /**
   * Rotate a node.
   *
   * @param {Object} node The node to be rotated, default to the active node.
   * @param {number} deg The rotate degrees.
   * @param {boolean} notify Whether to notify to record history.
   */
  rotateNode(node, deg, notify = true) {
    let activeNode = this.getActive('node');
    node = node || activeNode;

    if (!node) {
      return;
    }

    if (deg === 0) {
      return;
    }

    node.shape.rotate(deg);
    this._tick({node});

    if (notify) {
      this._pushHistory('rotateNode', {node, deg});
    }
  }

  /**
   * Set a node's shape.
   *
   * @param {Object} node The node to be shaped, default to the active node.
   * @param {string} shapeName The new shape name.
   * @param {boolean} notify Whether to notify to record history.
   */
  setNodeShape(node, shapeName, label, notify = true) {
    let activeNode = this.getActive('node');
    node = node || activeNode;

    if (!node) {
      return;
    }

    let oldShape = node.shape,
        oldLabel = node.label;
    if (oldShape.shapeName === shapeName && oldLabel === label) {
      return;
    }

    if (!_.isEmpty(label)) {
      node.label = label;
    } else {
      delete node.label;
    }

    oldShape.destroy();
    node.shape = new nodeShapes[shapeName](node, this);
    node.shape.renderTo(oldShape.elem);

    this._tick({node});

    if (notify) {
      this._pushHistory('setNodeShape', {
        node,
        from: oldShape.shapeName,
        to: shapeName
      });
    }
  }

  /**
   * Set the position of a node's text.
   *
   * @param {Object} node The node to be rotated, default to the active node.
   * @param {string} position The new position.
   * @param {boolean} notify Whether to notify to record history.
   */
  setNodeTextPosition(node, position, notify = true) {
    let activeNode = this.getActive('node');
    node = node || activeNode;

    if (!node) {
      return;
    }

    let from = node.textPosition || DEFAULT_NODE_TEXT_POSITION;
    if (from === position) {
      return;
    }

    node.textPosition = position;
    node.shape.reRenderText();

    this._tick({node});

    if (notify) {
      this._pushHistory('setNodeTextPosition', {
        node,
        from,
        to: position
      });
    }
  }

  /**
   * Set the visibility of capacity.
   *
   * @param {boolean} visibility Whether to notify to record history.
   */
  setCapacityVisibility(visibility) {
    this.options.capacityVisibility = visibility;
    this.elements.node.each(node => {
      node.shape.setCapacityVisibility(visibility);
    });
    this._tick();
  }

  /**
   * Set capacity of all nodes
   *
   * @param {Array} [{appId: '511', capacity: 0.6}]
   */
  setCapacityVal(nodes, stateColor, colorDomain, colorRange) {
    this.elements.node.each(node => {
      let nd = _.find(nodes, n => _.eq(n.appId, node.appId));
      node.shape.setCapacityVal(nd && nd.capacity || null, stateColor, colorDomain, colorRange);
    });
    this._tick();
  }

  /**
   * Append a link.
   *
   * @param {Object} link
   * @param {boolean} notify Whether to notify to record history and reflow.
   */
  link(link = {}, notify = true) {
    this.links.push(link);
    // console.log(`created link ${link.source[this.keyId]} => ${link.target[this.keyId]}`);

    if (notify) {
      this._pushHistory('link', {link});
      this.reflow(false, {link});
    }
  }

  /**
   * Remove a link.
   *
   * @param {Object} link
   * @param {boolean} notify Whether to notify to record history and reflow.
   */
  unlink(link, notify = true) {
    let activeLink = this.getActive('link');
    link = link || activeLink;

    if (!link) {
      return;
    }

    _.remove(this.links, link);
    // console.log(`removed link ${link.source[this.keyId]} => ${link.target[this.keyId]}`);

    if (notify) {
      this._pushHistory('unlink', {link});
      this.reflow(false, {link});
    }

    if (link === activeLink) {
      this.unsetActive();
    }
  }

  /**
   * Move a node forward.
   *
   * @param {Object} node The node to be forwarded, default to the active node.
   * @param {boolean} notify Whether to notify to record history and reflow.
   */
  forwardNode(node, notify = true) {
    let activeNode = this.getActive('node');
    node = node || activeNode;

    if (!node) {
      return;
    }

    const index = _.findIndex(this.nodes, node);
    if (index >= this.nodes.length - 1) {
      return;
    }

    this.nodes.splice(index, 1);
    this.nodes.splice(index + 1, 0, node);

    if (notify) {
      this._pushHistory('forwardNode', {node});
      this.reflow(true, {node});
    }

    if (node === activeNode) {
      this.emit('activeNodeChange', node);
    }
  }

  /**
   * Move a node backward.
   *
   * @param {Object} node The node to be backwarded, default to the active node.
   * @param {boolean} notify Whether to notify to record history and reflow.
   */
  backwardNode(node, notify = true) {
    let activeNode = this.getActive('node');
    node = node || activeNode;

    if (!node) {
      return;
    }

    const index = _.findIndex(this.nodes, node);
    if (index <= 0) {
      return;
    }

    this.nodes.splice(index, 1);
    this.nodes.splice(index - 1, 0, node);

    if (notify) {
      this._pushHistory('backwardNode', {node});
      this.reflow(true, {node});
    }

    if (node === activeNode) {
      this.emit('activeNodeChange', node);
    }
  }

  /**
   * Append an area.
   *
   * @param {Object} area
   * @param {number} index Which index the area should be appended at.
   * @param {boolean} notify Whether to notify to record history and reflow.
   */
  appendArea(area, index = this.areas.length, notify = true) {
    if (!area.areaId) {
      area.areaId = _.uniqueId('area');
    }
    this.areas.splice(index, 0, area);
    // console.log(`appended area ${area[this.keyId]}`);

    if (notify) {
      this._pushHistory('appendArea', {area, index});
      this.reflow(false, {area});
    }
  }

  /**
   * Remove an area.
   *
   * @param {Object} area The area to be removed, default to the active area.
   * @param {boolean} notify Whether to notify to record history and reflow.
   */
  removeArea(area, notify = true) {
    let activeArea = this._activeType === 'area' && this._activeElem && this._activeElem.datum();
    area = area || activeArea;

    if (!area) {
      return;
    }

    let index = _.findIndex(this.areas, area);
    if (index === -1) {
      return;
    }

    _.remove(this.areas, area);
    if (notify) {
      this._pushHistory('removeArea', {area, index});
      this.reflow(false, {area});
    }

    if (area === activeArea) {
      this.unsetActive();
    }
  }

  /**
   * Append a note.
   *
   * @param {Object} note
   * @param {number} index Which index the note should be appended at.
   * @param {boolean} notify Whether to notify to record history and reflow.
   */
  appendNote(note, index = this.notes.length, notify = true) {
    if (!note.noteId) {
      note.noteId = _.uniqueId('note');
    }
    this.notes.splice(index, 0, note);
    // console.log(`appended note ${note[this.keyId]}`);
    // console.log({note});

    if (notify) {
      this._pushHistory('appendNote', {note, index});
      this.reflow(false, {note});
    }
  }

  /**
   * Remove an note.
   *
   * @param {Object} note The note to be removed, default to the active note.
   * @param {boolean} notify Whether to notify to record history and reflow.
   */
  removeNote(note, notify = true) {
    let activeNote = this._activeType === 'note' && this._activeElem && this._activeElem.datum();
    note = note || activeNote;

    if (!note) {
      return;
    }

    let index = _.findIndex(this.notes, note);
    if (index === -1) {
      return;
    }

    _.remove(this.notes, note);
    if (notify) {
      this._pushHistory('removeNote', {note, index});
      this.reflow(false, {note});
    }

    if (note === activeNote) {
      this.unsetActive();
    }
  }

  /**
   * Set an note's content.
   *
   * @param {Object} note The note to be modified, default to the active note.
   * @param {string} content The new content.
   * @param {boolean} notify Whether to notify to record history and reflow.
   */
  setNoteContent(note, content, notify = true) {
    let activeNote = this._activeType === 'note' && this._activeElem && this._activeElem.datum();
    note = note || activeNote;

    if (!note) {
      return;
    }

    if (note.content === content) {
      return;
    }

    let from = note.content;
    note.content = content;
    note.shape.reRenderText();

    if (notify) {
      this._pushHistory('setNoteContent', {note, from, to: content});
    }
  }

  /**
   * Push an action to history.
   *
   * @private
   * @param {string} action
   * @param {Object} data
   */
  _pushHistory(action, data) {
    let state = {action, data};
    this.history.push(state);
    this.emit('history', 'push', state);
  }

  /**
   * Undo the previous action.
   *
   * @returns {Object} Returns the previous action, or null if not found.
   */
  undo() {
    let state = this.history.undo();

    if (!state) {
      return null;
    }

    let {action, data} = state;
    let reorder = false;
    let reflow = true;
    let needActive = true;
    let reflowTarget = null;
    let {node, link, area, note} = data;

    switch (action) {
      case 'appendNode':
        this.removeNode(data.node, false);
        reflowTarget = {node};
        needActive = false;
        break;
      case 'removeNode':
        this.appendNode(data.node, data.index, false);
        reflowTarget = {node};
        if (data.links) {
          _.forEach(data.links, link => this.link(link, false));
        }
        reorder = true;
        break;
      case 'link':
        this.unlink(data.link, false);
        reflowTarget = {link};
        needActive = false;
        break;
      case 'unlink':
        this.link(data.link, false);
        reflowTarget = {link};
        break;
      case 'dragNode':
        _.assign(data.node, data.from);
        reflow = false;
        this._tick();
        break;
      case 'forwardNode':
        this.backwardNode(data.node, false);
        reflowTarget = {node};
        reorder = true;
        break;
      case 'backwardNode':
        this.forwardNode(data.node, false);
        reflowTarget = {node};
        reorder = true;
        break;
      case 'rotateNode':
        this.rotateNode(data.node, -data.deg, false);
        reflow = false;
        break;
      case 'setNodeShape':
        this.setNodeShape(data.node, data.from, false);
        reflow = false;
        break;
      case 'setNodeTextPosition':
        this.setNodeTextPosition(data.node, data.from, false);
        reflow = false;
        break;
      case 'appendArea':
        this.removeArea(data.area, false);
        reflowTarget = {area};
        needActive = false;
        break;
      case 'removeArea':
        this.appendArea(data.area, data.index, false);
        reflowTarget = {area};
        // reorder = true;
        break;
      case 'dragArea':
        _.assign(data.area, data.from);
        reflow = false;
        this._tick({area});
        break;
      case 'resizeArea':
        _.assign(data.area, data.from);
        reflow = false;
        this._tick({area});
        break;
      case 'appendNote':
        this.removeNote(data.note, false);
        reflowTarget = {note};
        needActive = false;
        break;
      case 'removeNote':
        this.appendNote(data.note, data.index, false);
        reflowTarget = {note};
        // reorder = true;
        break;
      case 'setNoteContent':
        this.setNoteContent(data.note, data.from, false);
        reflow = false;
        break;
      case 'dragNote':
        _.assign(data.note, data.from);
        reflow = false;
        this._tick();
        break;
      case 'dragNoteArrow':
        data.note.shape.setArrowDirection(data.from);
        reflow = false;
        break;
      default:
        throw new Error(`unknown history action: ${action}`);
    }

    if (reflow) {
      this.reflow(reorder, reflowTarget);
    }

    if (needActive) {
      if (data.note) {
        this.setActive('note', data.note);
      }
      if (data.area) {
        this.setActive('area', data.area);
      }
      if (data.link) {
        this.setActive('link', data.link);
      }
      if (data.node) {
        this.setActive('node', data.node);
      }
    }

    this.emit('history', 'undo', state);

    return state;
  }

  /**
   * Redo the next action.
   *
   * @returns {Object} Returns the next action, or null if not found.
   */
  redo() {
    let state = this.history.redo();

    if (!state) {
      return null;
    }

    let {action, data} = state;
    let reorder = false;
    let reflow = true;
    let needActive = true;
    let reflowTarget = null;
    let {node, link, area, note} = data;

    switch (action) {
      case 'appendNode':
        this.appendNode(data.node, data.index, false);
        reflowTarget = {node};
        reorder = true;
        break;
      case 'removeNode':
        this.removeNode(data.node, false);
        reflowTarget = {node};
        needActive = false;
        break;
      case 'link':
        this.link(data.link, false);
        reflowTarget = {link};
        break;
      case 'unlink':
        this.unlink(data.link, false);
        reflowTarget = {link};
        needActive = false;
        break;
      case 'dragNode':
        _.assign(data.node, data.to);
        reflow = false;
        this._tick({node});
        break;
      case 'forwardNode':
        this.forwardNode(data.node, false);
        reflowTarget = {node};
        reorder = true;
        break;
      case 'backwardNode':
        this.backwardNode(data.node, false);
        reflowTarget = {node};
        reorder = true;
        break;
      case 'rotateNode':
        this.rotateNode(data.node, data.deg, false);
        reflow = false;
        break;
      case 'setNodeShape':
        this.setNodeShape(data.node, data.to, false);
        reflow = false;
        break;
      case 'setNodeTextPosition':
        this.setNodeTextPosition(data.node, data.to, false);
        reflow = false;
        break;
      case 'appendArea':
        this.appendArea(data.area, data.index, false);
        reflowTarget = {area};
        // reorder = true;
        break;
      case 'removeArea':
        this.removeArea(data.area, false);
        reflowTarget = {area};
        needActive = false;
        break;
      case 'dragArea':
        _.assign(data.area, data.to);
        reflow = false;
        this._tick({area});
        break;
      case 'resizeArea':
        _.assign(data.area, data.to);
        reflow = false;
        this._tick({area});
        break;
      case 'appendNote':
        this.appendNote(data.note, data.index, false);
        reflowTarget = {note};
        // reorder = true;
        break;
      case 'removeNote':
        this.removeNote(data.note, false);
        reflowTarget = {note};
        needActive = false;
        break;
      case 'setNoteContent':
        this.setNoteContent(data.note, data.to, false);
        reflow = false;
        break;
      case 'dragNote':
        _.assign(data.note, data.to);
        reflow = false;
        this._tick();
        break;
      case 'dragNoteArrow':
        data.note.shape.setArrowDirection(data.to);
        reflow = false;
        break;
      default:
        throw new Error(`unknown history action: ${action}`);
    }

    if (reflow) {
      this.reflow(reorder, reflowTarget);
    }

    if (needActive) {
      if (data.note) {
        this.setActive('note', data.note);
      }
      if (data.area) {
        this.setActive('area', data.area);
      }
      if (data.link) {
        this.setActive('link', data.link);
      }
      if (data.node) {
        this.setActive('node', data.node);
      }
    }

    this.emit('history', 'redo', state);

    return state;
  }

  /**
   * Initialize the `links` when `link.source`s and `link.target`s
   * were not suitable with d3 force layout.
   */
  initLinks() {
    let id = this.keyId;

    let {nodes, links} = this;

    let keys = _.map(nodes, id);
    if (_.uniq(keys).length !== nodes.length) {
      throw new ReferenceError('duplicated ids in nodes');
    }

    let nodeMap = _.zipObject(keys, nodes);

    _.forEach(links, function(link){
      if (!nodeMap[link.source]) {
        throw new ReferenceError(`undefined link source node: ${link.source}`);
      } else if (!nodeMap[link.target]) {
        throw new ReferenceError(`undefined link target node: ${link.target}`);
      }

      link.source = nodeMap[link.source];
      link.target = nodeMap[link.target];
    });
  }
}
