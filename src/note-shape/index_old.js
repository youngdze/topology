import _ from 'lodash';
import {Intersection, IntersectionParams} from '../../externals/kld-intersections';
import {Point2D, Matrix2D} from '../../externals/kld-affine';

const DEFAULT_ARROW_DIRECTION = Math.PI * 3 / 4;
const DEFAULT_ARROW_ANGLE = Math.PI / 10;

export default class NoteShape {
  static DEFAULT_ARROW_DIRECTION = DEFAULT_ARROW_DIRECTION;
  static DEFAULT_ARROW_ANGLE = DEFAULT_ARROW_ANGLE;

  constructor(note, topo) {
    // note.content = 'Hello world!\n你好，世界！';
    if (!note.arrowDirection) {
      note.arrowDirection = DEFAULT_ARROW_DIRECTION;
    }
    this.note = note;
    this.topo = topo;
  }

  destroy() {
    this.elem.selectAll('*').remove();
  }

  renderTo(elem) {
    this.elem = elem;

    elem.append('path')
        .attr('class', 'note-bg');

    elem.append('text')
        .attr('class', 'note-text')
        .attr('x', 0)
        .attr('y', 0);

    elem.append('circle')
        .attr('class', 'arrow-handle');

    this.reRenderText();
  }

  reRenderText() {
    let note = this.note,
        text = this.elem.select('.note-text'),
        content = _.trim(note.content).replace(/\r?\n|\n/g, '\n'),
        lines = content ? content.split('\n') : [];

    text.selectAll('tspan').remove();

    let dy = 6 - (lines.length - 1) * 10;
    _.forEach(lines, function(line){
      text.append('tspan')
          .attr('class', 'note-tspan')
          .attr('x', 0)
          .attr('y', 0)
          .attr('dy', dy)
          .text(_.trim(line));
      dy += 20;
    });

    /*let arrowDrag = d3.behavior.drag()
        .on('dragstart', () => {
          d3.event.sourceEvent.stopPropagation();
        }).on('drag', () => {
          this._arrowDx += d3.event.dx;
          this._arrowDy += d3.event.dy;

          let angle = Math.atan(this._arrowDy / this._arrowDx);
          if (_.isNaN(angle)) {
            angle = DEFAULT_ARROW_DIRECTION;
          } else if (this._arrowDx < 0) {
            angle += Math.PI;
          }
          this.setArrowDirection(angle);
        });
    arrowHandle.call(arrowDrag);*/

    this.setArrowDirection();
  }

  setArrowDirection(direction = this.note.arrowDirection) {
    this.note.arrowDirection = direction;

    let elem = this.elem;

    let bbox = elem.select('.note-text').node().getBBox(),
        rx = bbox.width / 2 + 20,
        ry = bbox.height / 2 + 15;

    let arrowAngle = DEFAULT_ARROW_ANGLE,
        sin = Math.sin(direction),
        cos = Math.cos(direction),
        radius = Math.max(rx, ry) + 1;

    let arrowSize = 15;
    let line = IntersectionParams.newLine(
      new Point2D(0, 0),
      new Point2D(cos * radius, sin * radius)
    );
    let ellipse = IntersectionParams.newEllipse(new Point2D(0, 0), rx, ry);

    let inter = Intersection.intersectShapes(
      line,
      ellipse,
      Matrix2D.IDENTITY,
      Matrix2D.IDENTITY
    ).points[0];

    let arrow = new Point2D(
      Math.round(inter.x + arrowSize * cos),
      Math.round(inter.y + arrowSize * sin)
    );

    elem.select('.arrow-handle')
        .attr('cx', arrow.x)
        .attr('cy', arrow.y)
        .attr('r', 3.5);

    this._arrowX = arrow.x;
    this._arrowY = arrow.y;
    /*let arrowDx = arrow.x,
        arrowDy = arrow.y;
    let arrowDrag = d3.behavior.drag()
        .on('dragstart', () => {
          d3.event.sourceEvent.stopPropagation();
          // let [x, y] = d3.mouse(elem.node());
          // let dx = arrow.x - x,
          //     dy = arrow.y - y;
        }).on('drag', () => {
          arrowDx += d3.event.dx;
          arrowDy += d3.event.dy;

          let angle = Math.atan(arrowDy / arrowDx);
          if (_.isNaN(angle)) {
            angle = DEFAULT_ARROW_DIRECTION;
          } else if (arrowDx < 0) {
            angle += Math.PI;
          }
          this.setArrowDirection(angle);
        });
    arrowHandle.call(arrowDrag);*/

    // let a1 = direction + Math.PI - arrowAngle / 2,
    //     a2 = direction + Math.PI + arrowAngle / 2;
    let a1 = direction + arrowAngle / 2,
        a2 = direction - arrowAngle / 2;

    let l1 = IntersectionParams.newLine(
      // arrow,
      // new Point2D(arrow.x + Math.cos(a1) * radius, arrow.y + Math.sin(a1) * radius)
      new Point2D(0, 0),
      new Point2D(Math.cos(a1) * radius, Math.sin(a1) * radius)
    );
    let l2 = IntersectionParams.newLine(
      // arrow,
      // new Point2D(arrow.x + Math.cos(a2) * radius, arrow.y + Math.sin(a2) * radius)
      new Point2D(0, 0),
      new Point2D(Math.cos(a2) * radius, Math.sin(a2) * radius)
    );

    let i1 = Intersection.intersectShapes(
      l1,
      ellipse,
      Matrix2D.IDENTITY,
      Matrix2D.IDENTITY
    ).points;
    let i2 = Intersection.intersectShapes(
      l2,
      ellipse,
      Matrix2D.IDENTITY,
      Matrix2D.IDENTITY
    ).points;

    let path;
    if (!i1.length || !i2.length) {
      path = `M -${rx} 0 A ${rx} ${ry} 0 1 1 ${rx} 0 A ${rx} ${ry} 0 1 1 -${rx} 0`;
    } else {
      path = `M ${i1[0].x} ${i1[0].y} A ${rx} ${ry} 0 1 1 ${i2[0].x} ${i2[0].y} L ${arrow.x} ${arrow.y} z`;
    }

    elem.select('.note-bg')
        .attr('d', path);
  }
}
