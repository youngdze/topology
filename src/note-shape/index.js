import _ from 'lodash';
import {Intersection, IntersectionParams} from '../../externals/kld-intersections';
import {Point2D, Matrix2D} from '../../externals/kld-affine';

export default class NoteShape {
  constructor(note, topo) {
    // note.content = 'Hello world!\n你好，世界！';
    this.note = note;
    this.topo = topo;
  }

  destroy() {
    this.elem.selectAll('*').remove();
  }

  renderTo(elem) {
    this.elem = elem;

    elem.append('path')
        .attr('class', 'note-bg');

    elem.append('text')
        .attr('class', 'note-text')
        .attr('x', 0)
        .attr('y', 0);

    this.reRenderText();
  }

  reRenderText() {
    let note = this.note,
        text = this.elem.select('.note-text'),
        content = _.trim(note.content).replace(/\r?\n|\n/g, '\n'),
        lines = content ? content.split('\n') : [];

    text.selectAll('tspan').remove();

    let dy = 6 - (lines.length - 1) * 10;
    _.forEach(lines, function(line){
      text.append('tspan')
          .attr('class', 'note-tspan')
          .attr('x', 0)
          .attr('y', 0)
          .attr('dy', dy)
          .text(_.trim(line));
      dy += 20;
    });

    this.setFrame();
  }

  setFrame() {
    let elem = this.elem;
    const RADIUS = 5;

    let bbox = elem.select('.note-text').node().getBBox(),
        rx = bbox.width / 2 + 20,
        ry = bbox.height / 2 + 14;

    let arrowSideLen = 12;

    let genCircularRect = (x, y, rx, ry, radius = 0, arrowSideLen = null) => {
      let path = [
        `M${x - rx} ${y}`,
        `L${x - rx} ${y - ry + radius}`,
        `A${radius} ${radius} 0 0 1 ${x - rx + radius} ${y - ry}`,
        `L${x + rx - radius} ${y - ry}`,
        `A${radius} ${radius} 0 0 1 ${x + rx} ${y - ry + radius}`,
        `L${x + rx} ${y + ry - radius}`,
        `A${radius} ${radius} 0 0 1 ${x + rx - radius} ${y + ry}`,
        `L${x - rx + radius} ${y + ry}`,
        `A${radius} ${radius} 0 0 1 ${x - rx} ${y + ry - radius}`,
        'Z'
      ];

      if (arrowSideLen) {
        path.splice(7, 0, `L${x} ${y + ry}`);
        path.splice(8, 0, `L${x - arrowSideLen} ${y + ry + arrowSideLen}`);
        path.splice(9, 0, `L${x - arrowSideLen} ${y + ry + radius}`);
        path.splice(10, 0, `A${radius} ${radius} 0 0 0 ${x - arrowSideLen - radius} ${y + ry}`);
      }

      return path;
    };

    let path = genCircularRect(0, 0, rx, ry, RADIUS, arrowSideLen).join('');

    elem.select('.note-bg')
        .attr('d', path);
  }
}
