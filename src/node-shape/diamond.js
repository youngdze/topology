import Polygon from './polygon';

export default class Diamond extends Polygon {
  constructor(node, topo, {width = 40} = {}) {
    super(node, topo, {width, points: 4});
    this.shapeName = 'Diamond';
  }
}
