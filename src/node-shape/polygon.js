import _ from 'lodash';
import NodeShape from './node-shape';
import {IntersectionParams} from '../../externals/kld-intersections';
import {Point2D} from '../../externals/kld-affine';

export default class Polygon extends NodeShape {
  constructor(node, topo, {width = 43, points = 5} = {}) {
    let {height, length, angle, positions, top, right, bottom, left} = Polygon.compute(width, points);

    super(node, topo, {width, height});

    this.shapeName = 'Polygon';
    this.points = points;
    this.length = length;
    this.angle = angle;
    this.positions = positions;

    this._top = top;
    this._right = right;
    this._bottom = bottom;
    this._left = left;

    this.setBoundingRect();

    this.sourceShape = this.getIntersectionShape(topo.options.linkStartOffset);
    this.targetShape = this.getIntersectionShape(topo.options.linkEndOffset);
  }

  setBoundingRect() {
    let {_top, _right, _bottom, _left} = this;
    this.top = _top;
    this.right = _right;
    this.bottom = _bottom;
    this.left = _left;
  }

  static compute(width, points) {
    let angle = (points - 2) * Math.PI / points,
        half = angle / 2, // Half angle of corner
        sin = Math.sin(half),
        cos = Math.cos(half),
        length,
        height;
    if (points <= 6) {
      length = width / 2 / sin;
    } else if (points <= 10) {
      length = width / 2 / (cos + sin);
    } else {
      throw new Error('polygon with points more than 10 is not supported');
    }

    let diameter = height = length / cos;

    if (points % 2) {
      height = diameter / 2 + diameter / 2 * sin;
      // diameter = Math.sqrt(Math.pow(length / 2, 2) + Math.pow(height, 2));
    }

    let top = -length / 2 / Math.cos(angle / 2),
        currentAngle = (Math.PI - angle) / 2,
        x = 0,
        y = top,
        positions = [[x, y]];

    for (let i = 0; i < points; i += 1) {
      x += Math.cos(currentAngle) * length;
      y += Math.sin(currentAngle) * length;
      positions.push([x, y]);
      currentAngle += Math.PI - angle;
    }

    return {
      length,
      height,
      angle,
      positions,
      top,
      right: width / 2,
      bottom: height + top,
      left: -width / 2
    };
  }

  renderTo(elem) {
    this.elem = elem;

    this.beforeRender(elem);

    this.renderCapacity();

    elem.append('g')
      .attr('class', 'node-icon')
        .append('polygon')
        .attr('class', 'shape shape-polygon')
        .attr('points', _.map(this.positions, p => p.join(',')).join(' '));

    this.renderText();

    this.afterRender(elem);

    this.renderLabel();
  }

  getIntersectionShape(offset) {
    if (this.topo.options.capacityVisibility) {
      return this.getMaxIntersectionShape(offset);
    }

    const IPTYPE = IntersectionParams.TYPE;
    let {positions, angle, top, length} = this;

    let segments;
    if (offset) {
      let cos = Math.cos(angle / 2),
          sin = Math.sin(angle / 2),
          rx = offset,
          ry = offset,
          corner = cos * offset * 2,
          x0 = -cos * offset,
          y0 = top - sin * offset,
          a0 = Math.PI + angle / 2,
          delta = Math.PI - angle;
      segments = [];
      for (let i = 0; i < positions.length; i += 1) {
        let [x, y] = positions[i];
        segments.push(new IntersectionParams(IPTYPE.ARC, [new Point2D(x, y), rx, ry, 0, a0, delta]));
        a0 += delta;
        let a1 = a0 + angle / 2,
            x1 = x0 + Math.cos(a1) * corner,
            y1 = y0 + Math.sin(a1) * corner,
            a2 = a0 + Math.PI / 2,
            x2 = x1 + Math.cos(a2) * length,
            y2 = y1 + Math.sin(a2) * length;
        segments.push(new IntersectionParams(IPTYPE.LINE, [new Point2D(x1, y1), new Point2D(x2, y2)]));
        x0 = x2;
        y0 = y2;
      }
    } else {
      segments = _.map(positions, (p, i) => {
        let j = i === positions.length - 1 ? 0 : i + 1;
        let a1 = new Point2D(p[0], p[1]),
            a2 = new Point2D(positions[j][0], positions[j][1]);
        return new IntersectionParams(IPTYPE.LINE, [a1, a2]);
      });
    }

    return IntersectionParams.newPath(segments);
  }

  beforeRender(elem) {}

  afterRender(elem) {}
}
