import Polygon from './polygon';

export default class Hexagon extends Polygon {
  constructor(node, topo, {width = 40} = {}) {
    super(node, topo, {width, points: 6});
    this.shapeName = 'Hexagon';
  }
}
