import NodeShape from './node-shape';
import {IntersectionParams} from '../../externals/kld-intersections';
import {Point2D} from '../../externals/kld-affine';

export default class Circle extends NodeShape {
  constructor(node, topo, {radius = 20} = {}) {
    super(node, topo, {width: radius, height: radius});
    this.shapeName = 'Circle';

    this.radius = radius;

    this.setBoundingRect();

    this.setIntersectionShape();
  }

  setBoundingRect() {
    let {radius} = this;
    this.top = this.left = -radius;
    this.right = this.bottom = radius;
  }

  renderTo(elem) {
    this.elem = elem;

    this.renderCapacity();

    let radius = this.radius;

    elem.append('g')
      .attr('class', 'node-icon')
        .append('circle')
        .attr('class', 'shape shape-circle')
        .attr('cx', 0)
        .attr('cy', 0)
        .attr('r', radius);

    this.renderText();
  }

  getIntersectionShape(offset) {
    if (this.topo.options.capacityVisibility) {
      return this.getMaxIntersectionShape(offset);
    }

    let radius = this.radius;
    if (offset) {
      radius += offset;
    }
    return IntersectionParams.newCircle(new Point2D(0, 0), radius);
  }
}
