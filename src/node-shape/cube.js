import _ from 'lodash';
import Polygon from './polygon';

export default class Cube extends Polygon {
  constructor(node, topo, {width = 40} = {}) {
    super(node, topo, {width, points: 6});
    this.shapeName = 'Cube';

    this.pvAngle = 150;
    this.pvOffsetY = this.length * Math.cos((this.pvAngle / 2) / 180 * Math.PI);
    this.pvOffsetX = this.length * 1 / 8;

    this.rendered = false;
  }

  beforeRender(elem) {
    this.positions[0][1] = this.positions[1][1] - this.pvOffsetY;
    this.positions[3][1] = this.positions[2][1] + this.pvOffsetY;
    this.positions[6][1] = this.positions[1][1] - this.pvOffsetY;
    if (!this.rendered) {
      this.positions[1][0] -= this.pvOffsetX;
      this.positions[2][0] -= this.pvOffsetX;
      this.positions[4][0] += this.pvOffsetX;
      this.positions[5][0] += this.pvOffsetX;
    }
  }

  afterRender(elem) {
    let positions = _.assignIn([], this.positions),
        centerPos = {x: positions[0][0], y: positions[1][1] + this.pvOffsetY};
    let path = [
      `M${centerPos.x} ${centerPos.y}`,
      `L${positions[1][0]}, ${positions[1][1]}`,
      `M${centerPos.x} ${centerPos.y}`,
      `L${positions[3][0]}, ${positions[3][1]}`,
      `M${centerPos.x} ${centerPos.y}`,
      `L${positions[5][0]}, ${positions[5][1]}`
    ].join('');

    elem.select('.node-icon')
        .append('path')
        .attr('class', 'shape shape-line')
        .attr('d', path);

    this.rendered = true;
  }
}
