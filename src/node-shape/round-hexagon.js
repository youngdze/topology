import RoundPolygon from './round-polygon';

export default class RoundHexagon extends RoundPolygon {
  constructor(node, topo, {width = 40, radius = 8} = {}) {
    super(node, topo, {width, radius, points: 6});
    this.shapeName = 'RoundHexagon';
  }
}
