import NodeShape from './node-shape';
import {IntersectionParams} from '../../externals/kld-intersections';

export default class Rectangle extends NodeShape {
  constructor(node, topo, {width = 40, height = 30} = {}) {
    super(node, topo, {width, height});
    this.shapeName = 'Rectangle';

    this.setBoundingRect();

    this.setIntersectionShape();
  }

  setBoundingRect() {
    let {width, height} = this;
    this.right = width / 2;
    this.left = -this.right;
    this.bottom = height / 2;
    this.top = -this.bottom;
  }

  renderTo(elem) {
    this.elem = elem;

    this.renderCapacity();

    let {width, height} = this;

    elem.append('g')
      .attr('class', 'node-icon')
        .append('rect')
        .attr('class', 'shape shape-rectangle')
        .attr('x', this.left)
        .attr('y', this.top)
        .attr('width', width)
        .attr('height', height);

    this.renderText();
  }

  getIntersectionShape(offset) {
    if (this.topo.options.capacityVisibility) {
      return this.getMaxIntersectionShape(offset);
    }

    let {top, left, width, height} = this,
        shape;

    if (offset) {
      shape = IntersectionParams.newRoundRect(
          left - offset,
          top - offset,
          width + 2 * offset, height + 2 * offset,
          offset, offset);
    } else {
      shape = IntersectionParams.newRect(left, top, width, height);
    }
    return shape;
  }
}
