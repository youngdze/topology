import NodeShape from './node-shape';
import { IntersectionParams} from '../../externals/kld-intersections';
import {Point2D} from '../../externals/kld-affine';

export default class Triangle extends NodeShape {
  constructor(node, topo, {width = 40, height = Math.sqrt(3) / 2 * width} = {}) {
    super(node, topo, {width, height});
    this.shapeName = 'Triangle';

    this.edge = Math.sqrt(width * width / 4 + height * height);
    this.angleB = Math.atan(height / (width / 2));
    this.angleA = Math.PI - this.angleB * 2;

    let gravityAngle = this.gravityAngle = Math.PI / 2 - this.angleB +
          Math.atan(((this.edge / 2) - Math.cos(this.angleB) * width) / (Math.sin(this.angleB) * width));
    this.gravityOffsetY = height / 2 - Math.tan(gravityAngle) * width / 2;

    this.setBoundingRect();

    this.setIntersectionShape();
  }

  setBoundingRect() {
    let {width, height, gravityOffsetY} = this;
    this.right = width / 2;
    this.left = -this.right;
    this.bottom = height / 2 - gravityOffsetY;
    this.top = -height / 2 - gravityOffsetY;
  }

  renderTo(elem) {
    this.elem = elem;

    this.renderCapacity();

    elem.append('g')
      .attr('class', 'node-icon')
        .append('path')
        .attr('class', 'shape shape-triangle')
        .attr('d', `M 0 ${this.top}L ${this.right} ${this.bottom}H ${this.left}Z`);

    this.renderText();
  }

  getIntersectionShape(offset) {
    if (this.topo.options.capacityVisibility) {
      return this.getMaxIntersectionShape(offset);
    }

    let {top, right, bottom, left, angleA, angleB} = this,
        shape;
    if (offset) {
      let sinHalfA = Math.sin(angleA / 2),
          cosHalfA = Math.cos(angleA / 2),
          sinB = Math.sin(angleB),
          cosB = Math.cos(angleB);

      let rx = offset,
          ry = rx,
          x0 = - cosHalfA * rx,
          y0 = top - sinHalfA * rx,
          cx01 = 0,
          cy01 = top,
          angle01 = Math.PI + angleA / 2,
          delta01 = Math.PI - angleA,
          x1 = cosHalfA * rx,
          y1 = y0,
          x2 = right + sinB * rx,
          y2 = bottom - cosB * rx,
          cx23 = right,
          cy23 = bottom,
          angle23 = -(Math.PI / 2 - angleB),
          delta23 = Math.PI / 2 - angle23,
          x3 = right,
          y3 = bottom + rx,
          x4 = left,
          y4 = y3,
          cx45 = left,
          cy45 = bottom,
          angle45 = Math.PI / 2,
          delta45 = delta23,
          x5 = left + x0,
          y5 = bottom - sinHalfA * rx;
      let segments = [];
      const IPTYPE = IntersectionParams.TYPE;
      segments.push(new IntersectionParams(IPTYPE.ARC, [new Point2D(cx01, cy01), rx, ry, 0, angle01, delta01]));
      segments.push(new IntersectionParams(IPTYPE.LINE, [new Point2D(x1, y1), new Point2D(x2, y2)]));
      segments.push(new IntersectionParams(IPTYPE.ARC, [new Point2D(cx23, cy23), rx, ry, 0, angle23, delta23]));
      segments.push(new IntersectionParams(IPTYPE.LINE, [new Point2D(x3, y3), new Point2D(x4, y4)]));
      segments.push(new IntersectionParams(IPTYPE.ARC, [new Point2D(cx45, cy45), rx, ry, 0, angle45, delta45]));
      segments.push(new IntersectionParams(IPTYPE.LINE, [new Point2D(x5, y5), new Point2D(x0, y0)]));
      shape = IntersectionParams.newPath(segments);
    } else {
      shape = IntersectionParams.newPolygon([new Point2D(0, top), new Point2D(left, bottom), new Point2D(right, bottom)]);
    }
    return shape;
  }
}
