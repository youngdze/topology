import NodeShape from './node-shape';
import {IntersectionParams} from '../../externals/kld-intersections';
import {Point2D} from '../../externals/kld-affine';

export default class Sphere extends NodeShape {
  constructor(node, topo, {radius = 20} = {}) {
    super(node, topo, {width: radius, height: radius});
    this.shapeName = 'Sphere';

    this.radius = radius;

    this.setBoundingRect();

    this.setIntersectionShape();
  }

  setBoundingRect() {
    let {radius} = this;
    this.top = this.left = -radius;
    this.right = this.bottom = radius;
  }

  renderTo(elem) {
    this.elem = elem;

    this.renderCapacity();

    let {radius} = this,
        cx = 0,
        cy = 0;

    let calSphereCoor = (cx, cy, r, coor, axis) => {
      let a = 0,
          b = 0;
      if (axis === 'y') {
        a = Math.sqrt(Math.pow(r, 2) - Math.pow((coor - cy), 2)) + cx;
        b = -Math.sqrt(Math.pow(r, 2) - Math.pow((coor - cy), 2)) + cx;
      } else {
        a = Math.sqrt(Math.pow(r, 2) - Math.pow((coor - cx), 2)) + cy;
        b = -Math.sqrt(Math.pow(r, 2) - Math.pow((coor - cx), 2)) + cy;
      }
      return {a, b};
    };

    let y1 = cy - radius / 2 - 2,
        x1 = calSphereCoor(cx, cy, radius, y1, 'y'),
        y2 = cy + radius / 2 - 2,
        x2 = calSphereCoor(cx, cy, radius, y2, 'y'),
        ellipseCurv = {x: 1, y: 1 / 4};

    let path = [
      `M${cx} ${cy - radius}`,
      `A${radius * 4 / 7} ${radius} 0 0 1 ${cx} ${cy + radius}`,
      `A${radius * 4 / 7} ${radius} 0 0 1 ${cx} ${cy - radius}`,
      `L${cx} ${cy + radius}`,
      `M${x1.a} ${y1}`,
      `A${ellipseCurv.x} ${ellipseCurv.y} 0 0 1 ${x1.b} ${y1}`,
      `M${cx - radius} ${cy - 2}`,
      `A${ellipseCurv.x} ${ellipseCurv.y} 0 0 0 ${cx + radius} ${cy - 2}`,
      `M${x2.a} ${y2}`,
      `A${ellipseCurv.x} ${ellipseCurv.y} 0 0 1 ${x2.b} ${y2}`
    ].join('');

    elem.append('g')
      .attr('class', 'node-icon')
        .append('circle')
        .attr('class', 'shape shape-sphere')
        .attr('cx', cx)
        .attr('cy', cy)
        .attr('r', radius);

    elem.select('.node-icon')
      .attr('class', 'node-icon')
        .append('path')
        .attr('class', 'shape shape-line')
        .attr('d', path);

    this.renderText();

    this.renderLabel();
  }

  getIntersectionShape(offset) {
    if (this.topo.options.capacityVisibility) {
      return this.getMaxIntersectionShape(offset);
    }

    let radius = this.radius;
    if (offset) {
      radius += offset;
    }
    return IntersectionParams.newCircle(new Point2D(0, 0), radius);
  }
}
