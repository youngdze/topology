import _ from 'lodash';
import NodeShape from './node-shape';
import Polygon from './polygon';
import {IntersectionParams} from '../../externals/kld-intersections';
import {Point2D} from '../../externals/kld-affine';

export default class RoundPolygon extends NodeShape {
  constructor(node, topo, {width = 43, points = 5, radius = 8} = {}) {
    let {height, length, angle, positions, top, right, bottom, left} = Polygon.compute(width, points);

    super(node, topo, {width, height});

    this.shapeName = 'RoundPolygon';
    this.points = points;
    this.radius = radius;
    this.length = length;
    this.angle = angle;
    this.positions = positions;

    this._top = top;
    this._right = right;
    this._bottom = bottom;
    this._left = left;

    this.setBoundingRect();

    let handles = this.handles = [],
        a0 = Math.PI / 2 + angle / 2,
        cut = this.cut = radius / Math.tan(angle / 2);
    this.gap = radius / Math.sin(angle / 2);
    _.forEach(positions, function(p){
      let x1 = Math.cos(a0) * cut + p[0],
          y1 = Math.sin(a0) * cut + p[1],
          a1 = a0 - angle,
          x2 = Math.cos(a1) * cut + p[0],
          y2 = Math.sin(a1) * cut + p[1];
      handles.push([x1, y1]);
      handles.push([x2, y2]);
      a0 = a1 + Math.PI;
    });

    this.setIntersectionShape();
  }

  setBoundingRect() {
    let {_top, _right, _bottom, _left} = this;
    this.top = _top;
    this.right = _right;
    this.bottom = _bottom;
    this.left = _left;
  }

  renderTo(elem) {
    this.elem = elem;

    this.renderCapacity();

    let radius = this.radius;

    let list = [];
    _.forEach(this.handles, function(p, index){
      let x = p[0],
          y = p[1];
      if (index === 0) {
        list.push(`M${x} ${y}`);
      } else if (index % 2) {
        list.push(`A${radius} ${radius} 0 0 1 ${x} ${y}`);
      } else {
        list.push(`L${x} ${y}`);
      }
    });

    elem.append('g')
      .attr('class', 'node-icon')
        .append('path')
        .attr('class', 'shape shape-round-polygon')
        .attr('d', list.join(''));

    this.renderText();
  }

  getIntersectionShape(offset) {
    if (this.topo.options.capacityVisibility) {
      return this.getMaxIntersectionShape(offset);
    }

    const IPTYPE = IntersectionParams.TYPE;
    let {positions, handles, angle, top, length, radius, cut, gap} = this;

    let segments = [];
    if (offset) {
      let cos = Math.cos(angle / 2),
          sin = Math.sin(angle / 2),
          rx = radius + offset,
          ry = rx,
          corner = cos * rx * 2,
          x0 = -cos * offset - sin * cut,
          y0 = top - sin * offset + cos * cut,
          a0 = Math.PI + angle / 2,
          delta = Math.PI - angle;
      _.forEach(positions, function(p){
        let [x, y] = p,
            a1 = a0 + delta + angle / 2,
            x1 = x0 + Math.cos(a1) * corner,
            y1 = y0 + Math.sin(a1) * corner,
            a2 = a0 + delta + Math.PI / 2,
            x2 = x1 + Math.cos(a2) * (length - 2 * cut),
            y2 = y1 + Math.sin(a2) * (length - 2 * cut),
            a3 = a1 + Math.PI / 2;

        x += Math.cos(a3) * gap;
        y += Math.sin(a3) * gap;

        segments.push(new IntersectionParams(IPTYPE.ARC, [new Point2D(x, y), rx, ry, 0, a0, delta]));
        segments.push(new IntersectionParams(IPTYPE.LINE, [new Point2D(x1, y1), new Point2D(x2, y2)]));

        x0 = x2;
        y0 = y2;
        a0 += delta;
      });
    } else {
      let x0,
          y0;
      _.forEach(handles, function(p, index){
        let x1 = p[0],
            y1 = p[1];
        if (index % 2) {
          segments.push(IntersectionParams.newArc(new Point2D(x0, y0), new Point2D(x1, y1), radius, radius, 0, 0, 1));
        } else if (index !== 0) {
          segments.push(IntersectionParams.newLine(new Point2D(x0, y0), new Point2D(x1, y1)));
        }
        x0 = x1;
        y0 = y1;
      });
    }

    return IntersectionParams.newPath(segments);
  }
}
