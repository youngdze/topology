import _ from 'lodash';
import {Intersection, IntersectionParams} from '../../externals/kld-intersections';
import {Point2D, Matrix2D} from '../../externals/kld-affine';

const DEFAULT_TEXT_POSITION = 'bottom';

export default class NodeShape {
  static DEFAULT_TEXT_POSITION = DEFAULT_TEXT_POSITION;

  constructor(node, topo, {width = 40, height = 40} = {}) {
    this.node = node;
    this.topo = topo;
    this.width = width;
    this.height = height;
    this.capacityRadius = this.topo.options.capacityRadius;

    // this.textPosition = 'top';

    if (!node.transform) {
      node.transform = {};
    }
    _.defaults(node.transform, {
      translate: [0, 0],
      rotate: 0,
      scale: [1, 1],
      skew: 0
    });
  }

  destroy() {
    this.elem.selectAll('*').remove();
  }

  setBoundingRect() {}
  renderTo() {}
  getIntersectionShape() {}

  getMaxIntersectionShape(offset) {
    let radius = this.capacityRadius;
    if (offset) {
      radius += offset;
    }
    return IntersectionParams.newCircle(new Point2D(0, 0), radius);
  }

  renderLabel() {
    let {elem, node, shapeName} = this,
        {scaleLevel} = this.topo.elements;
    let {labelEnabled, labelVisibility} = this.topo.options;
    let width = elem.select('.shape').node().getBBox().width,
        height = elem.select('.shape').node().getBBox().height;

    if (!labelEnabled || !node.label) return;

    const padding = 3;

    let nodeLabel = elem
        .append('g')
        .attr('class', 'node-label')
        .classed('hidden', () => scaleLevel < 0.8);

    let path = nodeLabel
        .append('path')
        .attr('class', 'shape-label-bg');

    let text = nodeLabel
        .append('text')
        .attr('class', 'shape-label-text')
        .text(d => d.label);

    let offset = 0;
    switch (shapeName) {
      case 'Cylinder':
        offset = -5;
        break;
      case 'Cube':
        offset = -5;
        break;
      default:
        break;
    }

    let textBbox = text.node().getBBox();
    // The textBbox data isn't accurate when the canvas is zoom out or zoom in
    textBbox = {
      height: 15,
      width: 7.5 * node.label.length
    };
    text.attr('x', textBbox.width / 2 + padding)
        .attr('y', -height + textBbox.height * 2)
        .attr('dy', offset);

    let genCircularRect = (x, y, rx, ry, radius = 0) => {
      let path = [
        `M${x}, ${y}`,
        `m${-rx} 0`,
        `v${-(ry - radius)}`,
        `a${radius} ${radius} 0 0 1 ${radius} ${-radius}`,
        `h${rx * 2 - radius * 2}`,
        `a${radius} ${radius} 0 0 1 ${radius} ${radius}`,
        `v${ry * 2 - radius * 2}`,
        `a${radius} ${radius} 0 0 1 ${-radius} ${radius}`,
        `h${-(rx * 2 - radius * 2)}`,
        `a${radius} ${radius} 0 0 1 ${-radius} ${-radius}`,
        'z'
      ];

      return path;
    };

    let circularRect = {
      x: textBbox.width / 2 + padding,
      y: -height + textBbox.height * 2 - padding - 1 + offset,
      rx: textBbox.width / 2 + padding,
      ry: textBbox.height / 2 + padding,
      radius: 3
    };
    let d = genCircularRect(
      circularRect.x,
      circularRect.y,
      circularRect.rx,
      circularRect.ry,
      circularRect.radius
    ).join('');
    path.attr('d', d);

    this.capacityRadius = Math.sqrt(
      Math.pow((Math.abs(circularRect.x) + Math.abs(circularRect.rx) + 12), 2),
      Math.pow((Math.abs(circularRect.y) + Math.abs(circularRect.ry) + 12), 2)
    );
  }

  renderCapacity() {
    let {elem} = this;
    let {capacityEnabled} = this.topo.options;

    if (!capacityEnabled) {
      return;
    }

    let progress = this.node.capacity || 0;

    let r = this.capacityRadius,
        radiant = 5 / 2 * Math.PI - 2 * Math.PI * progress,
        largeArc = +(progress > 0.5),
        x1 = Math.cos(radiant) * r,
        y1 = Math.sin(radiant) * r;

    elem
      .append('circle')
      .attr('class', 'shape-capacity-bg')
      .attr('cx', 0)
      .attr('cy', 0)
      .attr('r', r);

    elem
        .append('path')
        .attr('class', 'shape-capacity-circle')
        .attr('d', `M0 -${r}A${r} ${r} 0 ${largeArc} 1 ${x1} ${y1}`);
  }

  _scaleColor(t, domain = [0, 0.33, 0.66, 1], range = ['#13A7D5', '#1CD521', '#DEB704', '#F22E20']) {
    let color = d3.scaleLinear()
      .domain(domain)
      .range(range)(t);
    return color;
  }

  setCapacityVal(val, stateColor = false, colorDomain, colorRange) {
    let {elem, node} = this;
    let {capacityEnabled} = this.topo.options;

    if (!capacityEnabled) {
      return;
    }

    let progress = this.node.capacity = val;

    let r = this.capacityRadius,
        radiant = 2 * Math.PI * progress,
        largeArc = +(progress > 0.5),
        x1 = Math.sin(radiant) * r,
        y1 = -Math.cos(radiant) * r;

    let path = [
      `M0 -${r}`,
      `A${r} ${r} 0 ${largeArc} 1 ${x1} ${y1}`
    ];
    if (progress === 1) {
      path = [
        'M0 0',
        `m-${r} 0`,
        `a${r},${r} 0 1 0 ${r * 2} 0`,
        `a${r},${r} 0 1 0 -${r * 2} 0`,
      ];
    }

    path = path.join('');

    elem
      .select('circle.shape-capacity-bg')
      .attr('r', this.capacityRadius);

    elem
      .select('path.shape-capacity-circle')
      .attr('d', path)
      .style('stroke', d => {
        let {capacity} = this.node,
            color;

        color = (stateColor && capacity) ? this._scaleColor(capacity, colorDomain, colorRange) : null;
        return color;
      });
  }

  setCapacityVisibility(visibility) {
    if (!this.node.capacity) return;

    this.elem.classed('visible', visibility);
    if (visibility) {
      this.top = this.left = -this.capacityRadius;
      this.right = this.bottom = this.capacityRadius;
    } else {
      this.setBoundingRect();
    }
    this.setIntersectionShape();
    this.reRenderText();
  }

  setIntersectionShape() {
    this.sourceShape = this.getIntersectionShape(this.topo.options.linkStartOffset);
    this.targetShape = this.getIntersectionShape(this.topo.options.linkEndOffset);
  }

  renderText() {
    this.text = this.elem.append('text')
        .attr('dy', 0.5)
        .attr('class', 'node-text')
        .text(d => d.name);

    this.reRenderText();
  }

  reRenderText() {
    let {node, text} = this,
        textY;
    switch (node.textPosition) {
      case 'top':
        textY = this.top - 14;
        break;
      case 'center':
        textY = 0;
        break;
      // case 'bottom':
      default:
        textY = this.bottom + 14;
    }

    text.attr('y', textY);

    let textRect = text.node().getBBox();

    let {linkStartOffset, linkEndOffset} = this.topo.options;
    linkStartOffset += 2;
    linkEndOffset += 2;
    this.textSourceShape = IntersectionParams.newRoundRect(
      textRect.x - linkStartOffset,
      textRect.y - linkStartOffset,
      textRect.width + linkStartOffset * 2,
      textRect.height + linkStartOffset * 2,
      linkStartOffset,
      linkStartOffset
    );

    this.textTargetShape = IntersectionParams.newRoundRect(
      textRect.x - linkEndOffset,
      textRect.y - linkEndOffset,
      textRect.width + linkEndOffset * 2,
      textRect.height + linkEndOffset * 2,
      linkEndOffset,
      linkEndOffset
    );
  }

  getBoundingRect() {
    let {node, width, height, top, right, bottom, left} = this;
    return {
      top: node.y + top,
      right: node.x + right,
      bottom: node.y + bottom,
      left: node.x + left,
      width,
      height
    };
  }

  /*getGuides(isHorizontal = false) {
    return isHorizontal ? this.getHorizontals() : this.getVerticals();
  }*/

  getVerticals() {
    let node = this.node;
    return {
      cx: node.x,
      // left: node.x + this.left,
      // right: node.x + this.right
    };
  }

  getHorizontals() {
    let node = this.node;
    return {
      cy: node.y,
      // top: node.y + this.top,
      // bottom: node.y + this.bottom
    };
  }

  setTranslate(x, y) {
    this.node.transform.translate = [x, y];
  }

  setRotate(deg) {
    this.node.transform.rotate = deg;
  }

  setScale(x, y = x) {
    this.node.transform.scale = [x, y];
  }

  setSkew(deg) {
    this.node.transform.skew = deg;
  }

  translate(x, y) {
    let translate = this.node.transform.translate;
    translate[0] += x;
    translate[1] += y;
  }

  rotate(deg) {
    this.node.transform.rotate += deg;
  }

  scale(x, y = x) {
    let scale = this.node.transform.scale;
    scale[0] *= x;
    scale[1] *= y;
  }

  skew(deg) {
    this.node.transform.skew += deg;
  }

  getAbsoluteTransform() {
    let node = this.node;
    let {translate, rotate, scale, skew} = node.transform;
    let {x, y} = node;
    if (translate) {
      x += translate[0];
      y += translate[1];
    }
    let string = `translate(${x},${y})`;
    if (rotate) {
      string += `rotate(${rotate})`;
    }
    if (skew) {
      string += `skewX(${skew})`;
    }
    if (scale) {
      string += `scale(${scale[0]},${scale[1]})`;
    }
    return string;
  }

  getIntersectionMatrix() {
    let node = this.node;
    let {translate, rotate, scale, skew} = node.transform;
    let {x, y} = node;
    if (translate) {
      x += translate[0];
      y += translate[1];
    }

    const r2d = Math.PI / 180;

    let matrix = Matrix2D.IDENTITY;
    matrix = matrix.translate(x, y);
    if (rotate) {
      matrix = matrix.rotate(r2d * rotate);
    }
    if (skew) {
      matrix = matrix.skewX(r2d * skew);
    }
    if (scale) {
      matrix = matrix.scaleNonUniform(scale[0], scale[1]);
    }

    return matrix;
  }

  getLinkPointAsSource(target, offsets = [0, 0], asSource = true) {
    let {node, sourceShape, targetShape, textSourceShape, textTargetShape} = this,
        targetNode = target.node,
        x1 = node.x + offsets[0],
        y1 = node.y + offsets[1],
        x2 = targetNode.x + offsets[0],
        y2 = targetNode.y + offsets[1];

    let line = IntersectionParams.newLine(new Point2D(x1, y1), new Point2D(x2, y2));

    let intersOfShape = Intersection.intersectShapes(
      line,
      asSource ? sourceShape : targetShape,
      Matrix2D.IDENTITY,
      node.shape.getIntersectionMatrix()
    ).points;

    let intersOfText;

    if (textSourceShape) {
      intersOfText = Intersection.intersectShapes(
        line,
        asSource ? textSourceShape : textTargetShape,
        Matrix2D.IDENTITY,
        node.shape.getIntersectionMatrix()
      ).points;
      // console.log(intersOfText);
    }

    let iShape;
    if (intersOfShape && intersOfShape.length) {
      iShape = {
        x1: intersOfShape[0].x,
        y1: intersOfShape[0].y
      };
      iShape.size = Math.sqrt(Math.pow(x1 - iShape.x1, 2) + Math.pow(y1 - iShape.y1, 2));
      // if (textSourceShape) {
      //   console.log('shape', x1 - iShape.x1, y1 - iShape.y1);
      //   // console.log('shape', Math.pow(x1 - iShape.x1, 2),  Math.pow(y1 - iShape.y1));
      // }
    }
    // if (textSourceShape) {
    //   console.log('before', iShape);
    // }
    if (intersOfText && intersOfText.length) {
      iShape = _.chain(intersOfText)
          .map(function(inter){
            return {
              x1: inter.x,
              y1: inter.y,
              size: Math.sqrt(Math.pow(x1 - inter.x, 2) + Math.pow(y1 - inter.y, 2))
            };
          })
          .concat(iShape)
          .maxBy('size')
          .value();
    }
    // if (textSourceShape) {
    //   console.log('after', iShape);
    // }

    return iShape || {
      x1,
      y1
    };
  }

  getLinkPointAsTarget(source, offsets = [0, 0]) {
    let point = this.getLinkPointAsSource(source, offsets, false);
    return {
      x2: point.x1,
      y2: point.y1
    };
  }
}
