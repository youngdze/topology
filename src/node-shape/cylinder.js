import NodeShape from './node-shape';
import {IntersectionParams} from '../../externals/kld-intersections';
import {Point2D} from '../../externals/kld-affine';

export default class Cylinder extends NodeShape {
  constructor(node, topo, {xRadius = 15, yRadius = 5, verticalHeight = 22} = {}) {
    let width = xRadius * 2,
        height = verticalHeight + yRadius * 2;
    super(node, topo, {width, height});
    this.shapeName = 'Cylinder';

    this.xRadius = xRadius;
    this.yRadius = yRadius;
    this.verticalHeight = verticalHeight;

    this.setBoundingRect();

    this.setIntersectionShape();
  }

  setBoundingRect() {
    let {width, height} = this;
    this.top = -height / 2;
    this.right = width / 2;
    this.bottom = height / 2;
    this.left = -width / 2;
  }

  renderTo(elem) {
    this.elem = elem;

    this.renderCapacity();

    let {xRadius, yRadius, verticalHeight, left, width} = this;

    let path = [
      `M${left} -${verticalHeight / 2}`,
      `a ${xRadius} ${yRadius} 0 1 1 ${width} 0`,
      `v ${verticalHeight}`,
      `a ${xRadius} ${yRadius} 0 1 1 -${width} 0`,
      'z',
    ].join('');

    elem.append('g')
      .attr('class', 'node-icon')
        .append('path')
        .attr('class', 'shape shape-cylinder')
        .attr('d', path);

    path = [
      `M${left} -${verticalHeight / 2}`,
      `a ${xRadius} ${yRadius} 0 1 0 ${width} 0`,
      `M${left} -${verticalHeight / 6}`,
      `a ${xRadius} ${yRadius} 0 1 0 ${width} 0`,
      `M${left} ${verticalHeight / 6}`,
      `a ${xRadius} ${yRadius} 0 1 0 ${width} 0`,
    ].join('');

    elem.select('.node-icon')
        .append('path')
        .attr('class', 'shape shape-line')
        .attr('d', path);

    this.renderText();

    this.renderLabel();
  }

  getIntersectionShape(offset) {
    if (this.topo.options.capacityVisibility) {
      return this.getMaxIntersectionShape(offset);
    }

    let {xRadius, yRadius, verticalHeight, left, width} = this,
        segments = [];

    if (offset) {
      xRadius += offset;
      yRadius += offset;
      left -= offset;
      width += offset * 2;
    }

    segments.push(IntersectionParams.newArc(
      new Point2D(left, -verticalHeight / 2),
      new Point2D(left + width, -verticalHeight / 2),
      xRadius,
      yRadius,
      0,
      1,
      1
    ));

    segments.push(IntersectionParams.newLine(
      new Point2D(left + width, -verticalHeight / 2),
      new Point2D(left + width, verticalHeight / 2)
    ));

    segments.push(IntersectionParams.newArc(
      new Point2D(left + width, verticalHeight / 2),
      new Point2D(left, verticalHeight / 2),
      xRadius,
      yRadius,
      0,
      1,
      1
    ));

    segments.push(IntersectionParams.newLine(
      new Point2D(left, verticalHeight / 2),
      new Point2D(left, -verticalHeight / 2)
    ));

    return IntersectionParams.newPath(segments);
  }
}
