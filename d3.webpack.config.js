// eslint-disable-next-line
'use strict';

let webpack = require('webpack');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let isProduction = (process.env.NODE_ENV === 'production');

let plugins = [
  new ExtractTextPlugin('[name].css')
];
if (isProduction) {
  plugins.push(new webpack.optimize.UglifyJsPlugin({
    compress: {
      drop_console: true,
      warnings: false
    }
  }));
}

module.exports = {
  entry: {
    d3: ['./src/_d3.js'],
  },
  output: {
    path: `${__dirname}/builds`,
    filename: 'd3.js',
    publicPath: 'builds/',
    libraryTarget: 'umd',
    library: 'd3'
  },
  devtool: 'source-map',
  plugins,
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        include: [
          `${__dirname}/src`,
        ],
      },
    ]
  },
};
