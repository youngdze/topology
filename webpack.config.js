// eslint-disable-next-line
'use strict';

let webpack = require('webpack');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let isProduction = (process.env.NODE_ENV === 'production');

let plugins = [
  new ExtractTextPlugin('[name].css')
];
if (isProduction) {
  plugins.push(new webpack.optimize.UglifyJsPlugin({
    compress: {
      drop_console: true,
      warnings: false
    }
  }));
}

module.exports = {
  entry: {
    'apps-topology': ['./index.js'],
  },
  output: {
    path: `${__dirname}/builds`,
    filename: 'apps-topology.js',
    publicPath: 'builds/',
    libraryTarget: 'umd',
    library: 'AppsTopology'
  },
  externals: {
    d3: 'd3',
    lodash: '_',
  },
  devtool: 'source-map',
  plugins,
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        include: [
          `${__dirname}/src`,
        ],
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader'),
        // loader: 'style!css!sass',
        include: `${__dirname}/src`,
      },
    ]
  },
};
