# Apps Topology

Create an editable topology diagram for apps's relationship, based on [D3](https://d3js.org/).

![examples](examples.gif)

## External dependencies

- [Lodash](https://lodash.com/)

## Internal dependencies

- [D3](https://d3js.org/)
- [Kld-intersections](https://github.com/thelonious/kld-intersections) (Modified)
- [Kld-affine](https://github.com/thelonious/kld-affine) (Modified)
- [Kld-polynomial](https://github.com/thelonious/kld-polynomial)
- [Mousetrap](https://github.com/ccampbell/mousetrap)

## Usage

```html
<!-- Lodash is not included in this library, you should include it mannually. -->
<script src="/PATH/TO/lodash.js"></script>

<!-- Include the main script and style. -->
<!-- It exported as a UMD module. -->
<link rel="stylesheet" href="builds/apps-topology.css">
<script src="builds/apps-topology.js"></script>

<script>
var data = {
    nodes: [ ... ],
    links: [ ... ]
};
var options = {
    renderTo: '#topo-container'
};
var topo = new AppsTopology(data, options);
</script>
```

## Prepare

```bash
npm install -g webpack webpack-dev-server
npm install
```

## Development

```bash
npm run serve

# Or
webpack-dev-server --progress --colors

# Then open the url `http://localhost:8080/webpack-dev-server/` in browser.
```

```bash
# Watch examples
npm run serve-examples

# Or
webpack-dev-server --config=examples/webpack.config.js --progress --colors
```

## build

```bash
npm run build

# Or
webpack --progress
```
